/********************************************************************/
/* Objetivo: Script para crear la tabla chavos                      */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */
/********************************************************************/
DROP TABLE CHAVOS CASCADE CONSTRAINTS ;

CREATE TABLE CHAVOS (
  NOMBRE            VARCHAR2 (30),
  APE_PATERNO       VARCHAR2 (30),
  APE_MATERNO       VARCHAR2 (30),
  TELEFONO          VARCHAR2 (15),
  CALLE             VARCHAR2 (30),
  NUM_EXTERIOR      VARCHAR2 (10),
  NUM_INTERIOR      VARCHAR2 (10),
  COLONIA           VARCHAR2 (30),
  DEL_MUNICIPIO     VARCHAR2 (30),
  CIUDAD            VARCHAR2 (30),
  ESTADO            VARCHAR2 (30),
  COD_POSTAL        VARCHAR2 (5)  NOT NULL,
  EMAIL             VARCHAR2 (40)  NOT NULL,
  CONTRASENIA       VARCHAR2 (16),
  FECHA_NACIMIENTO  DATE,
  SEXO              VARCHAR2 (1),
  ESCUELA           VARCHAR2 (30),
  NIVEL_ESCOLAR     VARCHAR2 (15),
  CARICATURA        VARCHAR2 (30),
  PASATIEMPO        VARCHAR  (100),
  FECHA_REGISTRO    DATE,
  CVE_PROMOCION     VARCHAR2 (30),
  CONTACTO          NUMBER(2),
  CONSTRAINT PK_CHAVOS
  PRIMARY KEY ( EMAIL )
    USING INDEX
     TABLESPACE SIICCDATOS1 PCTFREE 10
     STORAGE ( INITIAL 40960 NEXT 40960 PCTINCREASE 50 ))
   TABLESPACE SIICCDATOS1
   PCTFREE 10
   PCTUSED 40
   INITRANS 1
   MAXTRANS 255
  STORAGE (
   INITIAL 5242880
   NEXT 1048576
   PCTINCREASE 50
   MINEXTENTS 1
   MAXEXTENTS 1017
   FREELISTS 1 FREELIST GROUPS 1 )
   NOCACHE;

COMMIT;
/
