/********************************************************************/
/* Objetivo: Scrip para crear la tabla LOCALES                      */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/
DROP TABLE LOCALES;

CREATE TABLE LOCALES
(
   NUM_LOCAL       NUMBER(3)     NOT NULL
  ,TIENDA          VARCHAR2(50)  NOT NULL
  ,DESCRIPCION     VARCHAR2(300)
  ,PALABRAS        VARCHAR2(2000)
  ,GERENTE         VARCHAR2(50)
  ,TELEFONO_1      VARCHAR2(15)
  ,TELEFONO_2      VARCHAR2(15)
  ,FAX             VARCHAR2(15)
  ,URL             VARCHAR2(50)
  ,EMAIL           VARCHAR2(40)
  ,PISO            VARCHAR2(01)
  ,COORDENADAS     VARCHAR2(10)
  ,FOTO_FACHADA    VARCHAR2(30)
  ,FOTO_PROMOCION  VARCHAR2(30)
  ,DESC_PROM_1     VARCHAR2(200)
  ,DESC_PROM_2     VARCHAR2(200)
  ,CONSTRAINT pk_locales PRIMARY KEY (NUM_LOCAL)
)
STORAGE(INITIAL      500K
        NEXT          30K
        MINEXTENTS      1
        MAXEXTENTS   1017
        PCTINCREASE     0
        FREELISTS       1
        FREELIST GROUPS 1)
TABLESPACE SIICCDATOS1;

/********************************************************************/
/* Objetivo: Script para crear la tabla CATEGORIAS                  */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/
DROP TABLE CATEGORIAS;

CREATE TABLE CATEGORIAS
(
   CVE_CATEGORIA          NUMBER(3)
  ,DESCRIPCION            VARCHAR2(30)
  ,CONSTRAINT pk_categorias PRIMARY KEY (CVE_CATEGORIA)
)
STORAGE(INITIAL        1K
        NEXT           1K
        MINEXTENTS      1
        MAXEXTENTS   1017
        PCTINCREASE     0
        FREELISTS       1
        FREELIST GROUPS 1)
TABLESPACE SIICCDATOS1;

/********************************************************************/
/* Objetivo: Script para crear la tabla SUBCATEGORIAS               */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/

DROP TABLE SUBCATEGORIAS;

CREATE TABLE SUBCATEGORIAS
(
   CVE_SUBCATEGORIA       NUMBER(3)
  ,DESCRIPCION            VARCHAR2(30)
  ,CVE_CATEGORIA          NUMBER(3)
  ,CONSTRAINT pk_subcategorias PRIMARY KEY (CVE_SUBCATEGORIA)
)
STORAGE(INITIAL        1K
        NEXT           1K
        MINEXTENTS      1
        MAXEXTENTS   1017
        PCTINCREASE     0
        FREELISTS       1
        FREELIST GROUPS 1)
TABLESPACE SIICCDATOS1;

/********************************************************************/
/* Objetivo: Script para crear la tabla SECCIONES                   */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/

DROP TABLE SECCIONES;

CREATE TABLE SECCIONES
(
   CVE_SECCION            NUMBER(4)
  ,DESCRIPCION            VARCHAR2(30)
  ,CVE_SUBCATEGORIA       NUMBER(3)
  ,CONSTRAINT pk_secciones PRIMARY KEY (CVE_SECCION)
)
STORAGE(INITIAL        1K
        NEXT           1K
        MINEXTENTS      1
        MAXEXTENTS   1017
        PCTINCREASE     0
        FREELISTS       1
        FREELIST GROUPS 1)
TABLESPACE SIICCDATOS1;

/********************************************************************/
/* Objetivo: Scrip para crear la tabla DIRECTORIO                   */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/
DROP TABLE DIRECTORIO;

CREATE TABLE DIRECTORIO
(
   NUM_LOCAL              NUMBER(3)
  ,CVE_CATEGORIA          NUMBER(3)
  ,CVE_SUBCATEGORIA       NUMBER(3)
  ,CVE_SECCION            NUMBER(4)
  ,CONSTRAINT pk_directorio UNIQUE (NUM_LOCAL
                                   ,CVE_CATEGORIA
                                   ,CVE_SUBCATEGORIA
                                   ,CVE_SECCION)
)
PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
STORAGE(INITIAL        5K
        NEXT           1K
        MINEXTENTS      1
        MAXEXTENTS   1017
        PCTINCREASE     0
        FREELISTS       1
        FREELIST GROUPS 1)
TABLESPACE SIICCDATOS1;

