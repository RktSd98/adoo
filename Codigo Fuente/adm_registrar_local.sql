CREATE OR REPLACE PROCEDURE ADM_REGISTRAR_LOCAL(p_id               NUMBER
                                               ,p_num_local        NUMBER
                                               ,p_tienda           VARCHAR2
                                               ,p_descripcion      VARCHAR2
                                               ,p_palabras         VARCHAR2
                                               ,p_gerente          VARCHAR2
                                               ,p_telefono_1       VARCHAR2
                                               ,p_telefono_2       VARCHAR2
                                               ,p_fax              VARCHAR2
                                               ,p_url              VARCHAR2
                                               ,p_email            VARCHAR2
                                               ,p_piso             VARCHAR2
                                               ,p_coordenadas      VARCHAR2
                                               ,p_foto_fachada     VARCHAR2
                                               ,p_foto_promocion   VARCHAR2
                                               ,p_desc_prom_1      VARCHAR2
                                               ,p_desc_prom_2      VARCHAR2) AS

   /* Declaracion de variables */
   v_local    NUMBER(3);

BEGIN

   /* Determinamos cual sera la condicion a tomar */
   IF(p_id = 0) THEN
      v_local := p_num_local;
   ELSE
      v_local := p_id;

      BEGIN
         UPDATE locales
            SET num_local      = p_num_local
               ,tienda         = p_tienda
               ,descripcion    = p_descripcion
               ,palabras       = p_palabras
               ,gerente        = p_gerente
               ,telefono_1     = p_telefono_1
               ,telefono_2     = p_telefono_2
               ,fax            = p_fax
               ,url            = p_url
               ,email          = p_email
               ,piso           = p_piso
               ,coordenadas    = p_coordenadas
               ,foto_fachada   = p_foto_fachada
               ,foto_promocion = p_foto_promocion
               ,desc_prom_1    = p_desc_prom_1
               ,desc_prom_2    = p_desc_prom_2
          WHERE num_local = v_local;

         COMMIT;

         /* Presenta la consulta de locales */
         owa_util.redirect_url('adm_mostrar_locales',FALSE);

      EXCEPTION
         WHEN DUP_VAL_ON_INDEX THEN
         ROLLBACK;
         htp.p('ERROR : Intento de modificar con un n'||'&'||'uacute;mero de local que ya existe');
		 RETURN;
      END;
   END IF;

   /* Inserta registro en la tabla locales */
   INSERT INTO locales
        VALUES(p_num_local
              ,p_tienda
              ,p_descripcion
              ,p_palabras
              ,p_gerente
              ,p_telefono_1
              ,p_telefono_2
              ,p_fax
              ,p_url
              ,p_email
              ,p_piso
              ,p_coordenadas
              ,p_foto_fachada
              ,p_foto_promocion
              ,p_desc_prom_1
              ,p_desc_prom_2);

   COMMIT;

   /* Presenta la consulta de locales */
   owa_util.redirect_url('adm_mostrar_locales',FALSE);

EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      BEGIN
         UPDATE locales
            SET num_local      = p_num_local
               ,tienda         = p_tienda
               ,descripcion    = p_descripcion
               ,palabras       = p_palabras
               ,gerente        = p_gerente
               ,telefono_1     = p_telefono_1
               ,telefono_2     = p_telefono_2
               ,fax            = p_fax
               ,url            = p_url
               ,email          = p_email
               ,piso           = p_piso
               ,coordenadas    = p_coordenadas
               ,foto_fachada   = p_foto_fachada
               ,foto_promocion = p_foto_promocion
               ,desc_prom_1    = p_desc_prom_1
               ,desc_prom_2    = p_desc_prom_2
          WHERE num_local = v_local;

         COMMIT;

         /* Presenta la consulta de locales */
         owa_util.redirect_url('adm_mostrar_locales',FALSE);

      EXCEPTION
         WHEN DUP_VAL_ON_INDEX THEN
         ROLLBACK;
         htp.p('ERROR : Intento de modificar con un n'||'&'||'uacute;mero de local que ya existe');
      END;

   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);
END;
/

