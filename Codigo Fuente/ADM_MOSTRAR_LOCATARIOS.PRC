CREATE OR REPLACE PROCEDURE adm_mostrar_locatarios AS

   /* Declaracion de variables */
   v_nombre                locatarios.nombre%TYPE;               
   v_ape_paterno          locatarios.APE_PATERNO%TYPE;              
   v_telefono          locatarios.TELEFONO%TYPE;            
   v_calle		          locatarios.CALLE%TYPE;			 			 
   v_num_exterior		locatarios.NUM_EXTERIOR%TYPE;			
	v_cod_postal		 locatarios.COD_POSTAL%TYPE;		
	v_email		          locatarios.EMAIL%TYPE; 			
	v_num_local		      locatarios.NUM_LOCAL%TYPE;	 
		  
   
   
   v_fecha_sistema         VARCHAR2(45);
   


   /* Variables auxiliares */
   v_inicio         NUMBER(3);
   v_contador       NUMBER(3) := 0;
   v_anterior       NUMBER(3);
   v_siguiente      NUMBER(3);
   v_max_num_produc NUMBER(3);
   v_min_num_produc NUMBER(3);
   v_aux            NUMBER(3);

   /* Declaracion de cursores */
   CURSOR mostrar_locatarios_cursor IS
          SELECT  lo.NOMBRE               
             ,lo.APE_PATERNO              
             ,lo.TELEFONO           
		     ,lo.CALLE			 			 
			 ,lo.NUM_EXTERIOR			
			 ,lo.COD_POSTAL		
			 ,lo.EMAIL 			
			 ,lo.NUM_LOCAL	 
		  
		  
		  
            FROM locatarios lo
          ORDER BY num_local;
BEGIN

   /* Mostrar Fecha */
    v_fecha_sistema := obtener_fecha;

   /* Indentifica si se selecciono PREVIO o SIGUIENTE */
--   IF(p_estado = 0) THEN
--      v_inicio := p_producto_prev;
--
--     ELSE
--      v_inicio := p_producto_next;
--   END IF;

   /* Obtenemos el valor m�ximo de num_local */
--   SELECT MAX(num_local)
--     INTO v_max_num_local
--     FROM locales;

   /* Obtenemos el valor m�nimo de num_local
   SELECT MIN(num_producto)
      INTO v_min_num_produc
      FROM productos;

   /* Abrir cursor */
   OPEN mostrar_locatarios_cursor;

   /* Recuperar el primer registro */
   FETCH mostrar_locatarios_cursor
    INTO v_nombre
	     ,v_ape_paterno                 
   ,v_telefono                     
   ,v_calle		          			 			 
   ,v_num_exterior					
    ,v_cod_postal			
	,v_email		    			
	,v_num_local;

   /* Variable Auxiliar para guardar primer num_local consultado */
--    v_aux:= p_local_prev;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top" class="textos">'||v_fecha_sistema||'</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="secciones">Administrador de Locatarios</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
  <tr>
    <td valign="top">
      <form name ="agregar" method="get" action="/administrador/adm_productos_add.htm">
        <input type="submit" value="Agregar Locatario">
      </form>
    </td>
  </tr>
</table>

<table width="780" border="1" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100" align="center" class="subtitulos3">Nombre</td>
    <td align="center" class="subtitulos3">Apellido paterno</td>
	<td align="center" class="subtitulos3">Codigo postal</td>
    <td align="center" class="subtitulos3">Email</td>
    <td align="center" class="subtitulos3">No. de Local</td>
    
	<td width="80">'||'&'||'nbsp;</td>
    <td width="80">'||'&'||'nbsp;</td>
  </tr>
');

   /* Realiza mientras haya registros */
   WHILE (mostrar_locatarios_cursor%FOUND)
   LOOP
      /* Muestra resultados de la busqueda */
   htp.p('
  <tr>
    
    <td class="textos">'||v_nombre||'</td>
	<td class="textos">'||v_ape_paterno||'</td>
	<td class="textos">'||v_cod_postal||'</td>
	<td class="textos">'||v_email||'</td>
	<td class="textos">'||v_num_local||'</td>
    <td width="80"><a href="adm_recuperar_producto?p_num_producto='||v_nombre||'" class="links">Modificar</a></td>
    <td width="80"><a href="adm_valida_borrar_locatario?p_email='||v_email||'" class="links">Borrar</a></td>
  </tr>
');

      /* Recuperar otro registro */
      FETCH mostrar_locatarios_cursor
       INTO  v_nombre
	     ,v_ape_paterno                 
   ,v_telefono                     
   ,v_calle		          			 			 
   ,v_num_exterior					
    ,v_cod_postal			
	,v_email		    			
	,v_num_local;
   END LOOP;

   /* Cerrar Cursor */
   CLOSE mostrar_locatarios_cursor;

   htp.p('
</table>
</BODY>
</HTML>
');


END;
/
