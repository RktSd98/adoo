/********************************************************************/
/* Script para crear la tabla paginas                               */
/*                                                                  */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/

DROP TABLE PAGINAS CASCADE CONSTRAINTS; 

CREATE TABLE PAGINAS
( 
  CVE_PAGINA   NUMBER (3)     NOT NULL, 
  DESCRIPCION  VARCHAR2 (30)  NOT NULL, 
  CONSTRAINT PK_PAGINAS PRIMARY KEY (CVE_PAGINA)
) 
PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
STORAGE (INITIAL        5K
         NEXT           1K
         MINEXTENTS      1
         MAXEXTENTS   1017
         PCTINCREASE     0
         FREELISTS       1
         FREELIST GROUPS 1)
TABLESPACE SIICCDATOS1;
COMMIT;

