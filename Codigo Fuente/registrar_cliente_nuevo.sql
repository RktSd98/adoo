/********************************************************************/
/* Objetivo: Procedure REGISTRAR_CLIENTES                             */
/********************************************************************/

CREATE OR REPLACE PROCEDURE registrar_clientes(p_nombre           VARCHAR2
                         ,p_ape_paterno      VARCHAR2
                         ,p_ape_materno      VARCHAR2
                         ,p_rango_edad       NUMBER
                         ,p_fecha_cumple     VARCHAR2
                         ,p_genero           VARCHAR2
                         ,p_estado_civil     NUMBER
                         ,p_colonia          VARCHAR2
                         ,p_cod_postal       VARCHAR2
                         ,p_ocupacion        NUMBER
                         ,p_grado            NUMBER
                         ,p_email            VARCHAR2
                         ,p_conf_email       VARCHAR2
                         ,p_contacto         NUMBER
                         ,p_promocion        VARCHAR2
                         ,p_pasatiempo       VARCHAR2
                         ,p_contrasena       VARCHAR2
                         ,p_conf_contrasena  VARCHAR2) AS

   /* Declaracion de variables */
   v_str_estado_civil VARCHAR2(30);
   v_str_ocupacion    VARCHAR2(30);
   v_str_escolaridad  VARCHAR2(30);

BEGIN

    /* Identificamos el estado civil */
   IF(p_estado_civil=1) THEN
      v_str_estado_civil := 'Soltero';
   ELSIF(p_estado_civil=2) THEN
      v_str_estado_civil := 'Casado';
   ELSIF(p_estado_civil=3) THEN
      v_str_estado_civil := 'Viudo';
   ELSIF(p_estado_civil=4) THEN
      v_str_estado_civil := 'Divorciado';
   END IF;

   /* Identificamos ocupacion */
   IF(p_ocupacion=1) THEN
      v_str_ocupacion := 'Estudiante';
   ELSIF(p_ocupacion=2) THEN
      v_str_ocupacion := 'Tecnico';
   ELSIF(p_ocupacion=3) THEN
      v_str_ocupacion := 'Comerciante';
   ELSIF(p_ocupacion=4) THEN
      v_str_ocupacion := 'Profesionista';
   ELSIF(p_ocupacion=5) THEN
      v_str_ocupacion := 'Empleado P�blico';
   ELSIF(p_ocupacion=6) THEN
      v_str_ocupacion := 'Empleado Privado';
   ELSIF(p_ocupacion=7) THEN
      v_str_ocupacion := 'Empresario';
   ELSIF(p_ocupacion=8) THEN
      v_str_ocupacion := 'Ama de Casa';
   ELSIF(p_ocupacion=9) THEN
      v_str_ocupacion := 'Jubilado';
   END IF;

   /* Identificamos escolaridad */
   IF(p_grado=1) THEN
      v_str_escolaridad := 'Primaria';
   ELSIF(p_grado=2) THEN
      v_str_escolaridad := 'Secundaria';
   ELSIF(p_grado=3) THEN
      v_str_escolaridad := 'Bachillerato';
   ELSIF(p_grado=4) THEN
      v_str_escolaridad := 'Carrera Tecnica';
   ELSIF(p_grado=5) THEN
      v_str_escolaridad := 'Licenciatura';
   ELSIF(p_grado=6) THEN
      v_str_escolaridad := 'Maestr�a';
   ELSIF(p_grado=7) THEN
      v_str_escolaridad := 'Doctorado';
   END IF;

   /* Inserta registro en la tabla clientes */
   INSERT INTO clientes
   (nombre,ape_paterno,ape_materno,cve_edad,cumpleanios,sexo
   ,estado_civil,colonia,cod_postal,ocupacion,escolaridad,email
   ,contacto,cve_promocion,contrasenia,fecha_registro)
   VALUES(p_nombre
         ,p_ape_paterno
         ,p_ape_materno
         ,p_rango_edad
         ,p_fecha_cumple
         ,p_genero
         ,v_str_estado_civil
         ,p_colonia
         ,p_cod_postal
         ,v_str_ocupacion
         ,v_str_escolaridad
         ,p_email
         ,p_contacto
         ,p_promocion
         ,p_contrasena
         ,SYSDATE);

   COMMIT;

   /* Envia pagina de bienvenida */
   owa_util.redirect_url('../mundo/07_mundo_bienvenida.htm',FALSE);

EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      /* Actualiza registro */
      UPDATE clientes c
         SET c.nombre       = p_nombre
            ,c.ape_paterno  = p_ape_paterno
            ,c.ape_materno  = p_ape_materno
            ,c.cve_edad     = p_rango_edad
            ,c.cumpleanios  = p_fecha_cumple
            ,c.sexo         = p_genero
            ,c.estado_civil = v_str_estado_civil
            ,c.colonia      = p_colonia
            ,c.cod_postal   = p_cod_postal
            ,c.ocupacion    = v_str_ocupacion
            ,c.escolaridad  = v_str_escolaridad
            ,c.contacto     = p_contacto
            ,c.cve_promocion= p_promocion
	        ,c.contrasenia  = p_contrasena
       WHERE c.email        = p_email;

      COMMIT;

      /* Envia pagina de bienvenida */
      owa_util.redirect_url('../mundo/07_mundo_bienvenida.htm',FALSE);

   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('Error en procedimiento: '||SQLCODE);

END;
/
