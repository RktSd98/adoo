CREATE OR REPLACE PROCEDURE ADM_BORRAR_LOCAL (p_num_local NUMBER) IS

BEGIN
   /* Borra registro de la tabla LOCALES */
   DELETE FROM locales
    WHERE num_local = p_num_local;

   COMMIT;

   /* Borra registros de la tabla DIRECTORIO */
   DELETE FROM directorio
    WHERE num_local = p_num_local;

   COMMIT;

   /* Borra registros de la tabla NOVEDADES */
   DELETE FROM novedades
    WHERE num_local = p_num_local;

   COMMIT;

   /* Presenta consulta */
   owa_util.redirect_url('adm_mostrar_locales',FALSE);

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

