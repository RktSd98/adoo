/********************************************************************/
/* Script para crear la tabla escolaridad                           */
/*                                                                  */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/
DROP TABLE ESCOLARIDAD CASCADE CONSTRAINTS ; 

CREATE TABLE ESCOLARIDAD ( 
  CVE_ESCOLARIDAD  NUMBER (3)    NOT NULL, 
  DESCRIPCION    VARCHAR2 (30), 
  CONSTRAINT PK_ESCOLARIDAD
  PRIMARY KEY ( CVE_ESCOLARIDAD ) 
    USING INDEX 
     TABLESPACE SIICCDATOS1 PCTFREE 10
     STORAGE ( INITIAL 40960 NEXT 40960 PCTINCREASE 50 ))
   TABLESPACE SIICCDATOS1
   PCTFREE 10
   PCTUSED 40
   INITRANS 1
   MAXTRANS 255
  STORAGE ( 
   INITIAL 5242880
   NEXT 1048576
   MINEXTENTS 1
   MAXEXTENTS 1017
   FREELISTS 1 FREELIST GROUPS 1 )
   NOCACHE; 

/********************************************************************/
/* Script para generar la tabla edades                              */
/*                                                                  */
/*                                                                  */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/

DROP TABLE EDADES CASCADE CONSTRAINTS ; 

CREATE TABLE EDADES ( 
  CVE_EDAD     NUMBER (3)    NOT NULL, 
  DESCRIPCION  VARCHAR2 (30)  NOT NULL, 
  CONSTRAINT PK_EDADES
  PRIMARY KEY ( CVE_EDAD ) 
    USING INDEX 
     TABLESPACE SIICCDATOS1 PCTFREE 10
     )
   TABLESPACE SIICCDATOS1 NOLOGGING 
   PCTFREE 10
   PCTUSED 40
   INITRANS 1
   MAXTRANS 255
   STORAGE ( 
   INITIAL 16384
   MINEXTENTS 1
   MAXEXTENTS 2147483645
   FREELISTS 1 FREELIST GROUPS 1 )
   NOCACHE; 

/********************************************************************/
/* Objetivo: Script para crear la tabla OCUPACIONES                 */
/*                                                                  */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/



 
DROP TABLE OCUPACIONES CASCADE CONSTRAINTS ; 

CREATE TABLE OCUPACIONES ( 
  CVE_OCUPACIONES  NUMBER (3)    NOT NULL, 
  DESCRIPCION    VARCHAR2 (30), 
  CONSTRAINT PK_OCUPACIONES
  PRIMARY KEY ( CVE_OCUPACIONES ) 
    USING INDEX 
     TABLESPACE SIICCDATOS1 PCTFREE 10
     STORAGE ( INITIAL 40960 NEXT 40960 PCTINCREASE 50 ))
   TABLESPACE SIICCDATOS1
   PCTFREE 10
   PCTUSED 40
   INITRANS 1
   MAXTRANS 255
  STORAGE ( 
   INITIAL 5242880
   NEXT 1048576
   MINEXTENTS 1
   MAXEXTENTS 1017
   FREELISTS 1 FREELIST GROUPS 1 )
   NOCACHE; 
/********************************************************************/
/* Objetivo: Script para crear la tabla SEVERIDAD                   */
/*                                                                  */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/

DROP TABLE SEVERIDAD CASCADE CONSTRAINTS ; 

CREATE TABLE SEVERIDAD ( 
  CVE_SEVERIDAD  NUMBER (3)    NOT NULL, 
  DESCRIPCION    VARCHAR2 (30), 
  CONSTRAINT PK_SEVERIDAD
  PRIMARY KEY ( CVE_SEVERIDAD ) 
    USING INDEX 
     TABLESPACE SIICCDATOS1 PCTFREE 10
     STORAGE ( INITIAL 40960 NEXT 40960 PCTINCREASE 50 ))
   TABLESPACE SIICCDATOS1
   PCTFREE 10
   PCTUSED 40
   INITRANS 1
   MAXTRANS 255
  STORAGE ( 
   INITIAL 5242880
   NEXT 1048576
   MINEXTENTS 1
   MAXEXTENTS 1017
   FREELISTS 1 FREELIST GROUPS 1 )
   NOCACHE; 

COMMIT;
/   

