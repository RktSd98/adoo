CREATE OR REPLACE FUNCTION obtener_fecha RETURN VARCHAR2 AS


   /* Declaraacion de variables */
   v_str_dia         VARCHAR2(1);
   v_str_mes         VARCHAR2(2);

   v_nombre_dia      VARCHAR2(16);
   v_nombre_mes      VARCHAR2(10);
   v_fecha_sistema   VARCHAR2(45);
   v_fecha_aux       VARCHAR2(8);


BEGIN
   /* Recuperamos numero de dia de la semana */
   SELECT TO_CHAR(sysdate, 'D') INTO v_str_dia FROM dual;

   /* Convertimos al nombre del dia */
   IF v_str_dia = '1' THEN
      v_nombre_dia := 'Domingo';
   END IF;
   IF v_str_dia = '2' THEN
      v_nombre_dia := 'Lunes';
   END IF;
   IF v_str_dia = '3' THEN
      v_nombre_dia := 'Martes';
   END IF;
   IF v_str_dia = '4' THEN
      v_nombre_dia := 'Mi'||'&'||'eacute;rcoles';
   END IF;
   IF v_str_dia = '5' THEN
      v_nombre_dia := 'Jueves';
   END IF;
   IF v_str_dia = '6' THEN
      v_nombre_dia := 'Viernes';
   END IF;
   IF v_str_dia = '7' THEN
      v_nombre_dia := 'S'||'&'||'aacute;bado';
   END IF;

   /* Obtener la fecha del sistema */
   SELECT TO_CHAR(sysdate,'DDMMYYYY')
     INTO v_fecha_aux
     FROM dual;

   /* Recuperamos nombre del mes */
   v_str_mes := SUBSTR(v_fecha_aux,3,2);

   /* Convertimos al nombre del mes */
   IF v_str_mes = '01' THEN
      v_nombre_mes := 'Enero';
   END IF;
   IF v_str_mes = '02' THEN
      v_nombre_mes := 'Febrero';
   END IF;
   IF v_str_mes = '03' THEN
      v_nombre_mes := 'Marzo';
   END IF;
   IF v_str_mes = '04' THEN
      v_nombre_mes := 'Abril';
   END IF;
   IF v_str_mes = '05' THEN
      v_nombre_mes := 'Mayo';
   END IF;
   IF v_str_mes = '06' THEN
      v_nombre_mes := 'Junio';
   END IF;
   IF v_str_mes = '07' THEN
      v_nombre_mes := 'Julio';
   END IF;
   IF v_str_mes = '08' THEN
      v_nombre_mes := 'Agosto';
   END IF;
   IF v_str_mes = '09' THEN
      v_nombre_mes := 'Septiembre';
   END IF;
   IF v_str_mes = '10' THEN
      v_nombre_mes := 'Octubre';
   END IF;
   IF v_str_mes = '11' THEN

      v_nombre_mes := 'Noviembre';
   END IF;
   IF v_str_mes = '12' THEN
      v_nombre_mes := 'Diciembre';
   END IF;

   /* Completar fecha del sistema */
   v_fecha_sistema := v_nombre_dia||', '||TO_NUMBER(SUBSTR(v_fecha_aux,1,2))||' de '||v_nombre_mes||' del '||SUBSTR(v_fecha_aux,5,4);

   RETURN v_fecha_sistema;

EXCEPTION
   WHEN OTHERS THEN
      v_fecha_sistema := 'Error al obtener fecha del sistema:'||SQLCODE;
      RETURN v_fecha_sistema;
END;
/

