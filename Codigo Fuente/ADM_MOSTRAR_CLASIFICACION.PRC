CREATE OR REPLACE PROCEDURE ADM_MOSTRAR_CLASIFICACION(p_num_local NUMBER
                                                     ,p_tienda VARCHAR2) AS

   /* Declaracion de variables para recuperar informacion de la Base de datos */
   v_cve_cat           directorio.CVE_CATEGORIA%TYPE;
   v_cve_subcat        directorio.CVE_SUBCATEGORIA%TYPE;
   v_cve_secc          directorio.CVE_SECCION%TYPE;
   v_cat_descripcion   categorias.DESCRIPCION%TYPE;
   v_sub_descripcion   subcategorias.DESCRIPCION%TYPE;
   v_sec_descripcion   secciones.DESCRIPCION%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema  VARCHAR2(45);

   /* Declaracion de cursores */
   CURSOR mostrar_directorio_cursor IS
      SELECT dir.CVE_CATEGORIA
            ,cat.DESCRIPCION
            ,dir.CVE_SUBCATEGORIA
            ,sub.DESCRIPCION
            ,dir.CVE_SECCION
            ,sec.DESCRIPCION
        FROM directorio dir, categorias cat, subcategorias sub, secciones sec
       WHERE dir.CVE_CATEGORIA    = cat.CVE_CATEGORIA
         AND dir.CVE_SUBCATEGORIA = sub.CVE_SUBCATEGORIA
         AND dir.CVE_SECCION      = sec.CVE_SECCION(+)
         AND dir.NUM_LOCAL        = p_num_local
        ORDER BY dir.CVE_CATEGORIA, dir.CVE_SUBCATEGORIA, dir.CVE_SECCION;
BEGIN

   /* Mostrar Fecha */
   v_fecha_sistema := obtener_fecha;

   /* Abrir cursor */
   OPEN mostrar_directorio_cursor;

   /* Recuperar el primer registro */
   FETCH mostrar_directorio_cursor
    INTO v_cve_cat
        ,v_cat_descripcion
        ,v_cve_subcat
        ,v_sub_descripcion
        ,v_cve_secc
        ,v_sec_descripcion;

   /* C�digo HTML est�tico dentro del procedure */
   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../../siicchtm/styles/estilos.css">
<BODY background="../../siicchtm/images/Fondo_adm.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../../siicchtm/images/Oracle.gif" width="125" height="20"></td>
    <td width="190" align="left" valign="top" class="textos">'||v_fecha_sistema||'</td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Administrador de Directorio ('||p_tienda||')</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../../siicchtm/images/lineah.gif" width="780" height="2"></td>
  </tr>
  <tr>
    <td valign="top">
      <form name ="agregar" method="get" action="adm_clasificacion_add1">
        <input type="hidden" name="p_tienda" value="'||p_tienda||'">
        <input type="hidden" name="p_num_local" value="'||p_num_local||'">
        <input type="submit" value="Agregar Clasificaci'||'&'||'oacute;n">
      </form>
    </td>
  </tr>
</table>

<table width="780" border="1" cellspacing="2" cellpadding="2">
  <tr>
    <td align="center" class="subtitulos3">Categor'||'&'||'iacute;a</td>
    <td align="center" class="subtitulos3">Subcategor'||'&'||'iacute;a</td>
    <td align="center" class="subtitulos3">Secci'||'&'||'oacute;n</td>
    <td>'||'&'||'nbsp;</td>
  </tr>
');

   /* Realiza mientras haya registros */
   WHILE (mostrar_directorio_cursor%FOUND)
   LOOP
      /* Revisar si la clave es cero Ninguna */
      IF(v_cve_subcat=0) THEN
         v_sub_descripcion := 'Ninguna';
      END IF;
      IF(v_cve_secc=0) THEN
         v_sec_descripcion := 'Ninguna';
      END IF;

      /* Muestra resultados de la busqueda */
   htp.p('
  <tr>
    <td class="textos">'||v_cat_descripcion||'</td>
    <td class="textos">'||v_sub_descripcion||'</td>
    <td class="textos">'||v_sec_descripcion||'</td>
    <td><a href="adm_valida_borrar_clasfccion?p_tienda='||p_tienda||'&'||'p_num_local='||p_num_local||'&'||'p_cve_cat='||v_cve_cat||'&'||'p_cve_subcat='||v_cve_subcat||'&'||'p_cve_secc='||v_cve_secc||'" class="links">Borrar</a></td>
  </tr>
');

      /* Recuperar otro registro */
      FETCH mostrar_directorio_cursor
       INTO v_cve_cat
           ,v_cat_descripcion
           ,v_cve_subcat
           ,v_sub_descripcion
           ,v_cve_secc
           ,v_sec_descripcion;
   END LOOP;

   /* Cerrar Cursor */
   CLOSE mostrar_directorio_cursor;

   htp.p('
</table>
</BODY>
</HTML>
');

END;
/
