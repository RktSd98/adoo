CREATE OR REPLACE PROCEDURE ADM_VALIDA_BORRAR_CLASFCCION(p_tienda     VARCHAR2
                                                        ,p_num_local  NUMBER
                                                        ,p_cve_cat    NUMBER
                                                        ,p_cve_subcat NUMBER
                                                        ,p_cve_secc   NUMBER) AS

   /* Variables para la recuperacion de datos de la base de datos */
   v_cat_descripcion   categorias.DESCRIPCION%TYPE;
   v_sub_descripcion   subcategorias.DESCRIPCION%TYPE;
   v_sec_descripcion   secciones.DESCRIPCION%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema  VARCHAR2(40);
   v_cont_reg       NUMBER(6);

BEGIN
   /* Obtenemos la fecha actual del sistema */
   v_fecha_sistema := obtener_fecha();

   /* Recuperamos descripciones */
   SELECT DESCRIPCION
     INTO v_cat_descripcion
     FROM categorias
    WHERE CVE_CATEGORIA = p_cve_cat;

   SELECT DECODE(RTRIM(DESCRIPCION), NULL, 'Ninguna', RTRIM(DESCRIPCION))
     INTO v_sub_descripcion
     FROM subcategorias
    WHERE CVE_SUBCATEGORIA = p_cve_subcat;

   SELECT COUNT(*)
     INTO v_cont_reg
     FROM secciones
    WHERE CVE_SECCION = p_cve_secc;

   IF(v_cont_reg = 0) THEN
      v_sec_descripcion := 'Ninguna';
   ELSE
      SELECT DECODE(RTRIM(DESCRIPCION), NULL, 'Ninguna', RTRIM(DESCRIPCION))
        INTO v_sec_descripcion
        FROM secciones
       WHERE CVE_SECCION = p_cve_secc;
   END IF;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top"><p class="textos">'||v_fecha_sistema||'</p></td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Eliminar clasificaci'||'&'||'oacute;n ('||p_tienda||')</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr>
    <td colspan="2" class="textos">Esta a punto de eliminar la siguiente clasificaci'||'&'||'oacute;n:</td>
  </tr>
  <tr>
    <td colspan="2" class="textos">Categor'||'&'||'iacute;a: '||v_cat_descripcion||'</td>
  </tr>
  <tr>
    <td colspan="2" class="textos">Subcategor'||'&'||'iacute;a: '||v_sub_descripcion||'</td>
  </tr>
  <tr>
    <td colspan="2" class="textos">Secci'||'&'||'oacute;n: '||v_sec_descripcion||'</td>
  </tr>
  <tr>
    <td colspan="2" align="center">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="textos">'||'&'||'iquest;Desea Continuar?</td>
  </tr>
  <tr>
    <td colspan="2" align="center">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <form name="form1" method="post" action="adm_borrar_clasificacion">
        <input type="hidden" name="p_tienda" value="'||p_tienda||'">
        <input type="hidden" name="p_num_local" value="'||p_num_local||'">
        <input type="hidden" name="p_cve_cat" value="'||p_cve_cat||'">
        <input type="hidden" name="p_cve_subcat" value="'||p_cve_subcat||'">
        <input type="hidden" name="p_cve_secc" value="'||p_cve_secc||'">
        <input type="submit" value="Aceptar">'||'&'||'nbsp;'||'&'||'nbsp;'||'&'||'nbsp;'||'&'||'nbsp;
		<input type="button" VALUE="Cancelar" onClick="history.go(-1)">
      </form>
    </td>
  </tr>
</table>
</BODY>
</HTML>
   ');

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

