CREATE OR REPLACE PROCEDURE BORRAR_TRACKING(p_valor  VARCHAR2
                                           ,p_num_local  NUMBER) IS
										   
						   

BEGIN



   /* Borra registro de la tabla PRODUCTOS*/
   DELETE FROM tracking
    WHERE p_valor = valor AND p_num_local = num_local;

   COMMIT;

   /* Presenta consulta */
   owa_util.redirect_url('mostrar_tracking?p_num_local='||p_num_local||'',FALSE);

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/
