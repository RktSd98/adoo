/********************************************************************/
/* Objetivo: Script para crear la tabla PATRONES_BUSQUEDA           */
/*   _____   _____   _____    _____    _____                        */
/*  / ____| |_   _| |_   _|  / ____|  / ____|                       */ 
/* | (___     | |     | |   | |      | |                            */
/*  \___ \    | |     | |   | |      | |                            */ 
/*  ____) |  _| |_   _| |_  | |____  | |____                        */
/* |_____/  |_____| |_____|  \_____|  \_____|                       */ 
/********************************************************************/
DROP TABLE PATRONES_BUSQUEDA CASCADE CONSTRAINTS ; 

CREATE TABLE PATRONES_BUSQUEDA ( 
  ID              NUMBER (7)    NOT NULL, 
  FECHA_REGISTRO  DATE, 
  PATRON          VARCHAR2 (200), 
  CONSTRAINT PK_PATRONES_BUSQUEDA
  PRIMARY KEY ( ID ) 
    USING INDEX 
     TABLESPACE SIICCDATOS1 PCTFREE 10
     STORAGE ( INITIAL 40960 NEXT 40960 PCTINCREASE 50 ))
   TABLESPACE SIICCDATOS1
   PCTFREE 10
   PCTUSED 40
   INITRANS 1
   MAXTRANS 255
  STORAGE ( 
   INITIAL 5242880
   NEXT 2097152
   MINEXTENTS 1
   MAXEXTENTS 1017
   FREELISTS 1 FREELIST GROUPS 1 )
   NOCACHE; 

COMMIT;
/   
