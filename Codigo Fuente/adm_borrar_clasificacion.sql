CREATE OR REPLACE PROCEDURE ADM_BORRAR_CLASIFICACION(p_tienda     VARCHAR2
                                                    ,p_num_local  NUMBER
                                                    ,p_cve_cat    NUMBER
                                                    ,p_cve_subcat NUMBER
                                                    ,p_cve_secc   NUMBER) AS

BEGIN
   /* Borra registro de la tabla */
   DELETE FROM directorio
    WHERE NUM_LOCAL = p_num_local
      AND CVE_CATEGORIA = p_cve_cat
      AND CVE_SUBCATEGORIA = p_cve_subcat
      AND CVE_SECCION = p_cve_secc;

   COMMIT;

   /* Presenta consulta */
   owa_util.redirect_url('adm_mostrar_clasificacion?p_num_local='||p_num_local||'&'||'p_tienda='||p_tienda,FALSE);

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

