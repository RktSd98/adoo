/********************************************************************/
/* Objetivo: Scrip para llenar la tabla SEVERIDAD                   */

insert into severidad
values (1,'Normal');
insert into severidad
values (2,'Dificil');
insert into severidad
values (3,'Grave');
insert into severidad
values (4,'Muy grave');
COMMIT;
