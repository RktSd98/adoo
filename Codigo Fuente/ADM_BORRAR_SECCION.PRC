CREATE OR REPLACE PROCEDURE ADM_BORRAR_SECCION(p_cve_subcategoria NUMBER
                                              ,p_cve_seccion      NUMBER) IS

   /* Variables para la recuperacion de informacion de la base de datos */
   v_num_registros   NUMBER(6);

   /* Variables auxiliares */
   v_fecha_sistema VARCHAR2(45);
BEGIN
   /* Revisar que no haya locales asociados con esta seccion */
   SELECT count(*)
     INTO v_num_registros
     FROM directorio
    WHERE CVE_SECCION = p_cve_seccion;

   IF(v_num_registros = 0) THEN
      /* Borra registro de la tabla */
      DELETE FROM secciones
       WHERE CVE_SECCION = p_cve_seccion;

      COMMIT;

      /* Presenta consulta */
      owa_util.redirect_url('adm_mostrar_secciones?p_cve_subcategoria='||p_cve_subcategoria,FALSE);
   ELSE
      htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../../siicchtm/styles/estilos.css">
<BODY background="../../siicchtm/images/Fondo_adm.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../../siicchtm/images/apache_pow.gif" width="259" height="32"></td>
    <td width="190" align="left" valign="top"><p class="textos">'||v_fecha_sistema||'</p></td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones" bgcolor="#FFFFFF">Eliminar Secci'||'&'||'oacute;n</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../../siicchtm/images/lineah.gif" width="780" height="2"></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr>
    <td colspan="2" class="textos" bgcolor="#FFFFFF">
      No se pudo borrar la secci'||'&'||'oacute;n, ya que, existen locales asociados a esta.<br>
      Para borrarla, asegurese que no haya ning'||'&'||'uacute;n local asociado a esta secci'||'&'||'oacute;n.
    </td>
  </tr>
  <tr>
    <td colspan="2">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <form name="form1" method="post" action="adm_mostrar_secciones">
		<input type="hidden" name="p_cve_subcategoria" value="'||p_cve_subcategoria||'">
		<input type="submit" value="Regresar">
      </form>
    </td>
  </tr>
</table>
</BODY>
</HTML>
      ');
   END IF;


EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/
