CREATE OR REPLACE PROCEDURE ADM_VALIDA_BORRAR_LOCAL(p_num_local NUMBER) IS

   /* Variables para la recuperacion de datos de la base de datos */
   v_num_local      locales.NUM_LOCAL%TYPE;
   v_tienda         locales.TIENDA%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema    VARCHAR2(40);

BEGIN
   /* Obtenemos la fecha actual del sistema */
   v_fecha_sistema := obtener_fecha();

   /* Consultar registro de la novedad */
   SELECT NUM_LOCAL
         ,TIENDA
     INTO v_num_local
         ,v_tienda
     FROM locales
    WHERE num_local = p_num_local;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top"><p class="textos">'||v_fecha_sistema||'</p></td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Eliminar local</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr>
    <td colspan="2" class="textos">Esta a punto de eliminar el siguiente local:</td>
  </tr>
  <tr>
    <td colspan="2" class="textos">Num. Local: '||v_num_local||'</td>
  </tr>
  <tr>
    <td width="200" class="textos">Tienda: '||v_tienda||'</td>
  </tr>
  <tr>
    <td colspan="2" align="center">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="textos">'||'&'||'iquest;Desea Continuar?</td>
  </tr>
  <tr>
    <td colspan="2" align="center">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <form name="form1" method="post" action="adm_borrar_local">
        <input type="hidden" name="p_num_local" value="'||p_num_local||'">
        <input type="submit" value="Aceptar">'||'&'||'nbsp;'||'&'||'nbsp;'||'&'||'nbsp;'||'&'||'nbsp;
		<input type="button" VALUE="Cancelar" onClick="history.go(-1)">
      </form>
    </td>
  </tr>
</table>
</BODY>
</HTML>
   ');

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

