CREATE OR REPLACE PROCEDURE adm_registrar_subcategoria(p_cve_categoria    NUMBER
                                                      ,p_cve_subcategoria NUMBER
                                                      ,p_descripcion      VARCHAR2)AS

BEGIN
   /* Se insertan los datos en la tabla */
   INSERT INTO subcategorias(CVE_CATEGORIA
                            ,CVE_SUBCATEGORIA
                            ,DESCRIPCION)
        VALUES(p_cve_categoria
              ,p_cve_subcategoria
              ,p_descripcion);

   /* Actualizaci�n de la base de datos */
   COMMIT;

   /* Presenta la consulta de eventos */
   owa_util.redirect_url('adm_mostrar_subcategorias?p_cve_categoria='||p_cve_categoria,FALSE);

/* Control de errores */
EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      UPDATE subcategorias
        SET  DESCRIPCION      = p_descripcion
       WHERE CVE_CATEGORIA    = p_cve_categoria
         AND CVE_SUBCATEGORIA = p_cve_subcategoria;

      /* Presenta la consulta de eventos */
      owa_util.redirect_url('adm_mostrar_subcategorias?p_cve_categoria='||p_cve_categoria,FALSE);

   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

