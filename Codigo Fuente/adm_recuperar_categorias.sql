CREATE OR REPLACE PROCEDURE ADM_RECUPERAR_CATEGORIA(p_cve_categoria NUMBER) IS

   /* Variables para la recuperacion de datos de la base de datos */
   v_descripcion   categorias.DESCRIPCION%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema    VARCHAR2(45);

BEGIN
   /* Obtenemos la fecha actual del sistema */
   v_fecha_sistema := obtener_fecha();

   /* Consultar registro */
   SELECT DESCRIPCION
     INTO v_descripcion
     FROM categorias
    WHERE CVE_CATEGORIA = p_cve_categoria;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<SCRIPT language=JavaScript>
<!--
function checkrequired(which){
   // Validar clave
   if (which.p_cve_categoria.value == "") {
      alert("Ingrese clave de categoria");
      which.p_cve_categoria.focus();
      return false;
   }

   // Validar descripcion
   if (which.p_descripcion.value == "") {
      alert("Ingrese descripcion");
      which.p_descripcion.focus();
      return false;
   }

   //Retorna True si pasa todas las validaciones
   return true;
}
//-->
</SCRIPT>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top">
      <p>'||v_fecha_sistema||'</p>
    </td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Administrador Directorio (Categor'||'&'||'iacute;as)</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
  <tr>
    <td colspan="2" height="5" align="center">
      <FORM name="register_form" action="adm_registrar_categoria" method="post" onsubmit="return checkrequired(this)">
        <table border="0" width="400" align="center">
          <tr>
            <td width="200" align="right" class="textos">Clave:</td>
            <td width="200">
              <input type="text" name="p_cve_categoria" value="'||p_cve_categoria||'" size="30" readonly>
            </td>
          </tr>
          <tr>
            <td width="200" align="right" class="textos">Descripci'||'&'||'oacute;n:</td>
            <td width="200">
              <input type="text" name="p_descripcion" value="'||v_descripcion||'" size="30" maxlength="30">
            </td>
          </tr>
          <tr>
            <td colspan="2" align="center">
              <input type="submit" value="Enviar">
              <input type="button" value="Cancelar" onClick="history.go(-1)">
            </td>
          </tr>
        </table>
      </FORM>
    </td>
  </tr>
</table>
</BODY>
</HTML>
   ');

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

