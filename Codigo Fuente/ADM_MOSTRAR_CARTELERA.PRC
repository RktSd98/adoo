CREATE OR REPLACE PROCEDURE ADM_MOSTRAR_CARTELERA(p_id     VARCHAR2
                                                 ,p_titulo VARCHAR2) AS

   /* Declaracion de variables para recuperar informacion de la Base de datos */
   v_cve_pelicula      peliculas.ID%TYPE;
   v_sala              cartelera.SALA%TYPE;
   v_funcion1          cartelera.FUNCION1%TYPE;
   v_funcion2          cartelera.FUNCION1%TYPE;
   v_funcion3          cartelera.FUNCION1%TYPE;
   v_funcion4          cartelera.FUNCION1%TYPE;
   v_funcion5          cartelera.FUNCION1%TYPE;
   v_funcion6          cartelera.FUNCION1%TYPE;
   v_funcion7          cartelera.FUNCION1%TYPE;
   v_funcion8          cartelera.FUNCION1%TYPE;
   v_funcion9          cartelera.FUNCION1%TYPE;
   v_funcion10         cartelera.FUNCION1%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema  VARCHAR2(40);

   /* Declaracion de cursores */
   CURSOR mostrar_cartelera_cursor IS
          SELECT  CVE_PELICULA
                 ,SALA
                 ,FUNCION1
                 ,FUNCION2
                 ,FUNCION3
                 ,FUNCION4
                 ,FUNCION5
                 ,FUNCION6
                 ,FUNCION7
                 ,FUNCION8
                 ,FUNCION9
                 ,FUNCION10
            FROM cartelera
           WHERE CVE_PELICULA = p_id
        ORDER BY SALA;
BEGIN

   /* Mostrar Fecha */
   v_fecha_sistema := obtener_fecha;

   /* Abrir cursor */
   OPEN mostrar_cartelera_cursor;

   /* Recuperar el primer registro */
   FETCH mostrar_cartelera_cursor
    INTO v_cve_pelicula
        ,v_sala
        ,v_funcion1
        ,v_funcion2
        ,v_funcion3
        ,v_funcion4
        ,v_funcion5
        ,v_funcion6
        ,v_funcion7
        ,v_funcion8
        ,v_funcion9
        ,v_funcion10;

   /* C�digo HTML  est�tico dentro del procedure */
   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../../siicchtm/styles/estilos.css">
<BODY background="../../siicchtm/images/Fondo_adm.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../../siicchtm/images/Oracle.gif" width="125" height="20"></td>
    <td width="190" align="left" valign="top" class="textos">'||v_fecha_sistema||'</td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones" bgcolor="#FFFFFF">Administrador de Cartelera ('||p_titulo||')</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../../siicchtm/images/lineah.gif" width="780" height="2"></td>
  </tr>
  <tr>
    <td valign="top">
      <form name ="agregar" method="get" action="adm_cartelera_add">
        <input type="hidden" name="p_cve_pelicula" value="'||p_id||'">
        <input type="hidden" name="p_titulo" value="'||p_titulo||'">
        <input type="submit" value="Agregar Cartelera">
      </form>
    </td>
  </tr>
</table>

<table width="780" border="1" cellspacing="2" cellpadding="2">
  <tr>
    <td align="center" class="subtitulos3">Sala</td>
    <td align="center" class="subtitulos3">Func. 1</td>
    <td align="center" class="subtitulos3">Func. 2</td>
    <td align="center" class="subtitulos3">Func. 3</td>
    <td align="center" class="subtitulos3">Func. 4</td>
    <td align="center" class="subtitulos3">Func. 5</td>
    <td align="center" class="subtitulos3">Func. 6</td>
    <td align="center" class="subtitulos3">Func. 7</td>
    <td align="center" class="subtitulos3">Func. 8</td>
    <td align="center" class="subtitulos3">Func. 9</td>
    <td align="center" class="subtitulos3">Func. 10</td>
    <td>'||'&'||'nbsp;</td>
    <td>'||'&'||'nbsp;</td>
  </tr>
');

   /* Realiza mientras haya registros */
   WHILE (mostrar_cartelera_cursor%FOUND)
   LOOP
      /* Muestra resultados de la busqueda */
   htp.p('
  <tr>
    <td class="textos">'||v_sala||'</td>
    <td class="textos">'||v_funcion1||'</td>
    <td class="textos">'||v_funcion2||'</td>
    <td class="textos">'||v_funcion3||'</td>
    <td class="textos">'||v_funcion4||'</td>
    <td class="textos">'||v_funcion5||'</td>
    <td class="textos">'||v_funcion6||'</td>
    <td class="textos">'||v_funcion7||'</td>
    <td class="textos">'||v_funcion8||'</td>
    <td class="textos">'||v_funcion9||'</td>
    <td class="textos">'||v_funcion10||'</td>
    <td><a href="adm_recuperar_cartelera?p_cve_pelicula='||p_id||'&'||'p_titulo='||p_titulo||'&'||'p_sala='||v_sala||'" class="links">Modificar</a></td>
    <td><a href="adm_valida_borrar_cartelera?p_cve_pelicula='||p_id||'&'||'p_titulo='||p_titulo||'&'||'p_sala='||v_sala||'" class="links">Borrar</a></td>
  </tr>
');

      /* Recuperar otro registro */
      FETCH mostrar_cartelera_cursor
       INTO v_cve_pelicula
           ,v_sala
           ,v_funcion1
           ,v_funcion2
           ,v_funcion3
           ,v_funcion4
           ,v_funcion5
           ,v_funcion6
           ,v_funcion7
           ,v_funcion8
           ,v_funcion9
           ,v_funcion10;
   END LOOP;

   /* Cerrar Cursor */
   CLOSE mostrar_cartelera_cursor;

   htp.p('
</table>
</BODY>
</HTML>
');

END;
/
