CREATE OR REPLACE PROCEDURE BUSCAR_PALABRA(keyword VARCHAR2
                                          ,v_compras NUMBER
										  ,p_valor_tracking VARCHAR2
										  ,p_num_tracking  VARCHAR2) AS

   /* Variables para el cursor */
   v_num_local      locales.num_local%TYPE;
   v_tienda         locales.tienda%TYPE;
   v_foto_fachada   locales.foto_fachada%TYPE;
   v_url            locales.url%TYPE;
   v_cve_catego     NUMBER(3);
   v_catego_des     VARCHAR2(30);
   v_cve_subcatego  NUMBER(3);
   v_subcatego_des  VARCHAR2(30);
   v_cve_seccion    NUMBER(4);
   v_seccion_des    VARCHAR2(30);
   v_producto1      VARCHAR(30);
   
   v_producto       productos.nombre%TYPE;
   
   p_num_compra     NUMBER(10):=2;

   /* Variables auxiliares */
   v_aux_foto_fachada locales.foto_fachada%TYPE;
   v_aux_url        locales.url%TYPE;
   v_aux_clas       VARCHAR2(90);
   v_clasificacion  VARCHAR2(90);
   v_fecha_sistema  VARCHAR2(45);

   v_p_url          locales.url%TYPE := '';
   corte            NUMBER(3);
   count1           NUMBER(3);
   count12          NUMBER(3);
   num_local_aux1   NUMBER(4);
   num_local_aux2   NUMBER(4);
   num_local_aux3   NUMBER(4);
   catego_aux       NUMBER(3);
   subcatego_aux    NUMBER(3);
   subsubcatego_aux NUMBER(4);
   shtml            VARCHAR2(500);
   source_cursor    NUMBER(2);
   ignore           NUMBER(2);
   v_sql_string     VARCHAR2(4000);
   v_aux_keyword    VARCHAR2(200);
   v_palabra        VARCHAR2(30);
   v_palabra_total  VARCHAR2(200);
   v_num_palabras   NUMBER(2) := 1;
   v_posicion       NUMBER(2);
   
   
   
      
   /*Variables auxiliares para el TRACKING*/
   v_valor_tracking VARCHAR2(1000);
   aux_track    VARCHAR2(3);
   p_num_producto NUMBER(7):=0;

   
   
BEGIN

 
   
   /*Obtener fecha sistema */
   v_fecha_sistema := obtener_fecha;
   
   /*Concatenar un caracter al tracking*/
   
 v_valor_tracking := p_valor_tracking || 'b';
 
    aux_track:=registrar_tracking(p_num_tracking,v_valor_tracking,v_fecha_sistema,v_num_local,p_num_producto);
   
   
   htp.p('
<HTML><HEAD>
<TITLE>SIICC IPN</TITLE>
</HEAD>

<script language="JavaScript">
function muestra(pFoto,pUrl,pClasificacion)
{
   //Armar el nombre del archivo del mapa de ubicacion a partir del nombre de la fachada

 
   for(i = pFoto.length; i >= 0; i--){
      if (pFoto.charAt(i)==".") break;
   }
   var v_foto_mapa=pFoto.substring(0,i) + "_m.gif";

   //Cambia mapa de ubicacion del local
  document["mapa"].src ="../../01directorio/01mapas/" + v_foto_mapa;


   //Cambia foto de la fachada del local
   //document["foto_fachada"].src ="../../01directorio/01locales/" + pFoto;


   // Armamos el string a escribir para la clasificacion
   // str = "<font face=Arial, Helvetica, sans-serif size=2><b>" + pClasificacion + "</b></font>"


   //Cambia clasificacion
   //if (document.layers) {
   //   with (document.clasifica.document) {
   //     open();
   //     write(str);
   //     close();
    //  }
 //  }
  // else {
    //  document.all.clasifica.innerHTML = str;
  // }

   //Cambia Url Foto
   if(pUrl.substring(0,3) == "www") {
     document.links[0].href = "menu?page=http://" + pUrl;
   }
   else {
      document.links[0].href = "http://#";
   }
}
//FIN DE MUESTRA()

function validaURL(which) {
   if (document.links[0].href == "http://#") {
      alert("No existe pagina");
      return false;
   }
}
</script>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY BACKGROUND="../00generales/00_back.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF" vlink="#DA004E" text="#122966" link="#FFFFFF" >

<DIV id="clasifica" style="position:absolute; width: 190px; height: 10px; top: 25px; left: 590px"></DIV>

<table width="775" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="5"></td>
<td valign="top" width="580">
<table width="580" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3">');
		   /* Realiza si existe o no URL */


   IF(SUBSTR(v_p_url,1,3) = 'www') THEN

	  htp.p('<a name="url_mapa" href="menu?page=http://'||v_p_url||'" target="_blank" onClick=" return validaURL(this)">');
   ELSE
      htp.p('<a name="url_mapa" href="http://#" target="_blank" onClick=" return validaURL(this)">');

   END IF;
		 htp.p('<img name="mapa" src="../../01directorio/01mapas/mapa_default.gif" width="780" height="250" border="0"></a>
</td>
</tr>
<tr>
<td width="190" valign="top">
<table width="195" border="0" cellspacing="0" cellpadding="0" bgcolor="#CB211A">
<td width="200" class="textos">'||p_valor_tracking||'</td>
<form name="search_form" action="buscar_palabra"
  
 metod="post" onSubmit="if(this.keyword.value=='''')
 {alert(''Introduzca una palabra''); return false } ; ">
<tr>

        <div align="left"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="258" height="41">
            <param name=movie value="../../siicchtm/flash/buscar.swf">
            <param name=quality value=high>
            <embed src="../../siicchtm/flash/buscar.swf" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="258" height="41">
            </embed> 
          </object></div>
     
<td width="200" class="textos">'||v_valor_tracking||'</td>
<td width="190" class="buscador1">
<div align="left">B'||'&'||'uacute;squedas</div>
</td>
</tr>
<tr>
<td width="190" align="center">
<span class="links">Por tienda o por palabra </span>
<input type="text" name="keyword" size="20" class="buscador2"><br>

<input type="hidden" name="v_compras" value="'||v_compras||'" >
<input type="hidden" name="p_valor_tracking" value="'||v_valor_tracking||'" >
<input type="hidden" name="p_num_tracking" value="'||p_num_tracking||'" >
			
<input type="submit" value="Buscar" class="buscador2" >
</td>
</tr>
</form>
</table></td>
<td width="5" background="../00generales/00_linea_v.gif"></td>
<td width="585" valign="top">
');

   /* Inicializamos variables */
   corte:=0;
   count1:=0;
   count12:=0;
   num_local_aux1 := -1;
   num_local_aux2 := -1;
   num_local_aux3 := -1;
   catego_aux := -1;
   subcatego_aux := -1;
   subsubcatego_aux := -1;
   shtml := '';
   v_cve_catego:=0;
/* Prepara el cursor dependiendo del tipo de consulta */
   source_cursor := dbms_sql.open_cursor;

   
   /* Arma consulta dinamicamente */
   v_sql_string := 'SELECT l.num_local, l.tienda, l.foto_fachada,l.url, c.cve_categoria, c.descripcion';

   v_sql_string := v_sql_string || ',s.cve_subcategoria,s.descripcion,sc.cve_seccion,sc.descripcion, p.nombre ';

   v_sql_string := v_sql_string || 'FROM locales l, directorio d, categorias c,subcategorias s, secciones sc, productos p ';

   v_sql_string := v_sql_string || 'WHERE d.num_local = l.num_local AND d.cve_categoria = c.cve_categoria ';

   v_sql_string := v_sql_string || 'AND d.cve_subcategoria = s.cve_subcategoria AND d.cve_seccion = sc.cve_seccion(+) ';

   v_sql_string := v_sql_string || 'AND p.num_local1 = l.num_local AND';
   
   /* Asigna la lista a una variable auxiliar */
   v_aux_keyword := UPPER(RTRIM(keyword));
   v_palabra_total:= v_aux_keyword;
		 
   /* Recuperar la o las palabras a buscar */
   WHILE(LENGTH(v_aux_keyword)>0)
   LOOP
      /* Revisar si hay mas de una palabra */
      v_posicion := INSTR(v_aux_keyword, ' ');

      IF (v_posicion > 0) THEN
         IF (v_num_palabras = 1) THEN
            v_sql_string := v_sql_string || '(';
         END IF;

         /* Incrementar */
         v_num_palabras := v_num_palabras + 1;

         /* Recupera la palabra */
         v_palabra := SUBSTR(v_aux_keyword, 1, v_posicion - 1);
		
		 
		 

         /* Eliminamos la palabra de la lista */
         v_aux_keyword := LTRIM(SUBSTR(v_aux_keyword, v_posicion + 1));

         /* Complementar la consulta con la palabra recuperada */
         v_sql_string := v_sql_string || 'UPPER(l.tienda)       LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(l.descripcion) LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(l.palabras)     LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(c.descripcion)  LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(s.descripcion)  LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(sc.descripcion) LIKE ''%'||UPPER(v_palabra)||'%'' OR ';
		 
		 v_sql_string := v_sql_string || 'UPPER(p.nombre)       LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

      ELSE
         IF v_num_palabras = 1 THEN
            v_sql_string := v_sql_string || '(';
         END IF;

         /* Recuperar palabra */
         v_palabra := v_aux_keyword;

         /* Complementar la consulta con la palabra recuperada */
         v_sql_string := v_sql_string || 'UPPER(l.tienda)       LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(l.descripcion)  LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(l.palabras)     LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(c.descripcion)  LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(s.descripcion)  LIKE ''%'||UPPER(v_palabra)||'%'' OR ';

         v_sql_string := v_sql_string || 'UPPER(sc.descripcion) LIKE ''%'||UPPER(v_palabra)||'%'' OR ';
		 
		 v_sql_string := v_sql_string || 'UPPER(p.nombre)       LIKE ''%'||UPPER(v_palabra)||'%'' ) ';


  /* Limpiamos la lista de palabras */
         v_aux_keyword := '';
      END IF;
   END LOOP;

   v_sql_string := v_sql_string || 'ORDER BY c.cve_categoria, s.cve_subcategoria, sc.cve_seccion, l.tienda';

   /* Asocia sql query string al cursor */
   DBMS_SQL.PARSE(source_cursor, v_sql_string, DBMS_SQL.NATIVE);

   /* Definimos variables que recibiran los valores de la consulta */
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 1, v_num_local);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 2, v_tienda,50);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 3, v_foto_fachada,30);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 4, v_url,50);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 5, v_cve_catego);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 6, v_catego_des,30);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 7, v_cve_subcatego);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 8, v_subcatego_des,30);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 9, v_cve_seccion);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 10, v_seccion_des,30);
   DBMS_SQL.DEFINE_COLUMN(source_cursor, 11, v_producto,20);

   /* Ejecutamos cursor */
   ignore := DBMS_SQL.EXECUTE(source_cursor);

   
   v_num_palabras:=1;
   /* Realiza mientras haya registros */
   
  
  
  
   LOOP
      /* Recuperamos registro del cursor */
      IF DBMS_SQL.FETCH_ROWS(source_cursor)>0 AND LENGTH(v_palabra_total)>0 THEN
         /* Recupera valores de las columnas */
         DBMS_SQL.COLUMN_VALUE(source_cursor, 1, v_num_local);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 2, v_tienda);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 3, v_foto_fachada);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 4, v_url);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 5, v_cve_catego);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 6, v_catego_des);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 7, v_cve_subcatego);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 8, v_subcatego_des);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 9, v_cve_seccion);
         DBMS_SQL.COLUMN_VALUE(source_cursor, 10, v_seccion_des);
		 DBMS_SQL.COLUMN_VALUE(source_cursor, 11, v_producto);
		 
		 
		 
		
		  /* Recuperar la o las palabras a buscar */
		  
  
  
  /* Revisar si hay mas de una palabra */
      v_posicion := INSTR(v_palabra_total, ' ');

      IF (v_posicion > 0) THEN
        
		 /* Recupera la palabra */
         v_palabra := SUBSTR(v_palabra_total, 1, v_posicion - 1);
		
		 
		 

         /* Eliminamos la palabra de la lista */
         v_palabra_total := LTRIM(SUBSTR(v_palabra_total, v_posicion + 1));
		  
		  
		   
		 END IF;
  
   
      
	
	IF (v_palabra = v_producto) THEN
		 v_producto1:= ' esta el producto '||v_producto;
	   ELSE 
	   v_producto1:=' ';
	END IF;
 
		 



      IF num_local_aux1 <> v_num_local AND
         num_local_aux2 <> v_num_local AND
         num_local_aux3 <> v_num_local  THEN

         IF v_num_local = 1 THEN
            num_local_aux1 := v_num_local;
            v_tienda := SUBSTR(v_tienda,1);
         END IF;

         IF v_num_local = 2 THEN
            num_local_aux2 := v_num_local;
            v_tienda := SUBSTR(v_tienda,1);
         END IF;
         IF v_num_local = 3 THEN
            num_local_aux3 := v_num_local;
            v_tienda := SUBSTR(v_tienda,1);
         END IF;
     /* Logica para controlar los cortes de categoria/subcategoria/seccion */
      corte := 0;

      IF catego_aux <> v_cve_catego THEN
         corte:=1;
         catego_aux := v_cve_catego;
         subcatego_aux := v_cve_subcatego;
         subsubcatego_aux := v_cve_seccion;
      ELSE
         IF subcatego_aux <> v_cve_subcatego THEN
            corte := 2;
            subcatego_aux := v_cve_subcatego;
            subsubcatego_aux := v_cve_seccion;
         ELSE
            IF subsubcatego_aux <> v_cve_seccion THEN
               corte := 3;
               subsubcatego_aux := v_cve_seccion;
            END IF;
         END IF;
      END IF;
      /* Logica para controlar los cierres de las tablas o renglones de la informacion obtenida */

      IF corte > 0 THEN
         count12 := 0;
      END IF;

      shtml := '';
      IF count1 > 0 THEN  -- cerrar tablas abiertas
         IF  corte > 0 THEN
            shtml := '</tr></table></tr></table><!-- CORTE (DE CUALQUIER TIPO -->';
         ELSE
            IF FLOOR(count12 / 3)*3 = count12 THEN
               shtml := '</tr><tr>';
            END IF;
         END IF;
      END IF;

      htp.p(shtml);

      /* Logica para controlar el desplegado de las descripciones de categoria/subcategoria/seccion */
      IF corte > 0 THEN
         IF corte = 1 THEN
            v_clasificacion := v_catego_des;
            --htp.p('
            --<!-- CORTE DE CATEGORIA --> <br>
--<font face="Arial, Helvetica, sans-serif" size="2" color="#000C62"><b>'||v_catego_des||'</b></font>
--');
         END IF;
         IF corte <= 2 AND v_subcatego_des IS NOT NULL THEN
            v_clasificacion := v_catego_des||'>'||v_subcatego_des;
            --htp.p('
            -- <!-- CORTE DE SUB_CATEGORIA --> <br>
--<img src="..img_gen/shim.gif" width="10" height="15">
--<font face="Arial, Helvetica, sans-serif" size="2" color="#122966"><b>'||v_subcatego_des||'</b></font>
--');
         END IF;
         IF corte <= 3 AND v_seccion_des IS NOT NULL THEN
            v_clasificacion := 'en la categoria '||v_catego_des||', subcategoria '||v_subcatego_des||' y seccion '||v_seccion_des||v_producto1;			
	

            --htp.p('
            --<!-- CORTE DE SUB_SUB_CATEGORIA --> <br>
--<img src="..img_gen/shim.gif" width="20" height="15">
--<font face="Arial, Helvetica, sans-serif" size="2" color="#122966">'||v_seccion_des||'</font>
--');
         END IF;

         /* Presenta ruta categoria>subcategoria>seccion */
         htp.p('<img src="../00generales/dot.gif" width="55" height="20">

<span  class="subtitulos">'||v_clasificacion||'</span>
');
         --shtml := '<!-- INICIA TABLA RESULTADOS --><table><tr><td><img src="..img_gen/shim.gif" width="30" height="15"></td><td><table><tr>';

         shtml := '<!-- INICIA TABLA RESULTADOS --><table><tr><td><table><tr>';
      END IF;
      IF count1 = 0 THEN
         v_aux_foto_fachada := v_foto_fachada;
         v_aux_url := v_url;
         v_aux_clas := v_clasificacion;
      END IF;

      /* Presenta nombre de la tienda */
      shtml := shtml || '<td width="128" class="textos">';
      shtml := shtml || '<a href="javascript:muestra('''||v_foto_fachada||''','''||v_url||''','''||v_clasificacion||''');" class="textos2" >'||v_tienda||'</a></td>';
	  
	  
	  
	 /* Verifica si es busqueda normal o busqueda de compras*/
	  IF(v_compras=1) THEN
	       shtml := shtml || '<a href="adm_mostrar_productos_local?num_local='||v_num_local||''||'&'||'p_num_compra='||p_num_compra||''||'&'||'p_valor_tracking='||v_valor_tracking||''||'&'||'p_num_tracking='||p_num_tracking||'" class="textos2" >'||v_num_local||'</a></td>';
		  
      END IF;
      
	  
	  htp.p(shtml);

      /* Incrementa cifras de control */
      corte := 0;
      count1 := count1 + 1;
      count12 := count12 + 1;
END IF;
      ELSE
         /* No encontro mas registros */
         EXIT;
      END IF;
   END LOOP;

   /* Cerramos el cursor */
   DBMS_SQL.CLOSE_CURSOR(source_cursor);

   IF (count1=0) THEN
      htp.p('<span class="subtitulos">
No se encontraron resultados para esta b'||'&'||'uacute;squeda.<br>
Para que que sea m'||'&'||'aacute;s efectiva s'||'&'||'oacute;lo tecle'||'&'||'eacute; una palabra.<br>
Intente de nuevo por favor.</span>
');
   ELSE
      /* Cierra tablas abiertas */
      shtml := '</tr></table></tr></table>';
      htp.p(shtml);
   END IF;

htp.p('
            </td>
</tr>
</table>
</td> ');
   /*   <td width="2" background="../00generales/00_linea_v.gif"></td>
<td width="190" valign="top">
<p><span class="secciones">Directorio</span></p>
<p><br><br></p>
<p align="center"><a name="url_foto" href="#" target="_blank" onClick="return validaURL(this)">
<img name="foto_fachada" src="../00generales/00_foto_default.gif" width="190" height="250" border="0"></a></p>
'); */
      IF (count1>0) THEN
         htp.p('
        <script>
muestra('''||v_aux_foto_fachada||''','''||v_aux_url||''','''||v_aux_clas||''')
        </script>
');
      END IF;

   /* </td> */
htp.p('</tr>
</table>
</BODY>
</HTML>
');

   registrar_patron(RTRIM(keyword));

END;
/

