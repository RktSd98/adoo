CREATE OR REPLACE PROCEDURE presentar_pelicula(p_id NUMBER) AS

   /* Variables para la recuperacion de datos de la base de datos */
   v_archivo          peliculas.ARCHIVO%TYPE;
   v_titulo           peliculas.TITULO%TYPE;
   v_director         peliculas.DIRECTOR%TYPE;
   v_guion            peliculas.GUION%TYPE;
   v_productor        peliculas.PRODUCTOR%TYPE;
   v_efectos          peliculas.EFECTOS%TYPE;
   v_musica           peliculas.MUSICA%TYPE;
   v_actores          peliculas.ACTORES%TYPE;
   v_duracion         peliculas.DURACION%TYPE;
   v_genero           peliculas.GENERO%TYPE;
   v_resenia          peliculas.RESENIA%TYPE;
   v_url_destino      peliculas.URL_DESTINO%TYPE;
   v_fecha_vig_inicio VARCHAR2(10);
   v_fecha_vig_fin    VARCHAR2(10);

BEGIN

   /* Consultar registro de la base de datos */
   SELECT ARCHIVO
         ,TITULO
         ,DIRECTOR
         ,GUION
         ,PRODUCTOR
         ,EFECTOS
         ,MUSICA
         ,ACTORES
         ,DURACION
         ,GENERO
         ,RESENIA
         ,URL_DESTINO
         ,TO_CHAR(FECHA_VIG_INICIO,'DD/MM/YYYY')
         ,TO_CHAR(FECHA_VIG_FIN,'DD/MM/YYYY')
     INTO v_archivo
         ,v_titulo
         ,v_director
         ,v_guion
         ,v_productor
         ,v_efectos
         ,v_musica
         ,v_actores
         ,v_duracion
         ,v_genero
         ,v_resenia
         ,v_url_destino
         ,v_fecha_vig_inicio
         ,v_fecha_vig_fin
     FROM peliculas
    WHERE id= p_id;

   htp.p('
<html>
<head>
<title>Galer'||'&'||'iacute;as Metepec</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language="JavaScript1.2">
function winclose(){
   window.close()
   self.close()
}
</script>
<link rel="stylesheet" href="../00styles/estilos.css">
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#122966" link="#da004e">
<table width="450" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="190">'||'&'||'nbsp;</td>
    <td width="260"><img src="../00superior/00imagenes1/00_menu_logo2.gif" width="87" height="108" align="top" name="Image1"></td>
  </tr>
  <tr>
    <td width="190" class="textos">
      <p align="center">
   ');

   IF(v_url_destino IS NULL) THEN
      htp.p('<img src="../04entretent/04imagenes/'||v_archivo||'" width="190" height="199">');
   ELSE
      htp.p('<a href="'||v_url_destino||'" onClick="window.close()" target="_blank"><img src="../04entretent/04imagenes/'||v_archivo||'" width="190" height="199" border="0"></a>');
   END IF;

   htp.p(v_titulo||'</p>
      <p align="center" class="textos"><b>Director</b>:'||v_director||'</p>
      <p align="center" class="textos"><b>Gui'||'&'||'oacute;n</b>:'||v_guion||'</p>
      <p align="center" class="textos"><b>Productores</b>:'||v_productor||'</p>
   ');

   IF(v_efectos IS NOT NULL) THEN
      htp.p('<p align="center" class="textos"><b>Efectos</b>:'||v_efectos||'</p>');
   END IF;
   IF(v_musica IS NOT NULL) THEN
      htp.p('<p align="center" class="textos"><b>M'||'&'||'uacute;sica</b>:'||v_musica||'</p>');
   END IF;

   htp.p('
      <p align="center" class="textos"><b>Actores</b>:'||v_actores||'</p>
      </td>
    <td width="260" class="normal" valign="top">
      <p>'||'&'||'nbsp;</p>
      <p class="textos">'||v_resenia||'</p>
      <p class="textos"><b>Duraci'||'&'||'oacute;n:</b>'||v_duracion||'</p>
      <p class="textos"><b>G'||'&'||'eacute;nero:</b>'||v_genero||'</p>
      <p class="normal" align="right">
	    <a href="http://www.cinepolis.com.mx" onClick="window.close()" target="_blank" class="links">Ir
        a Cin'||'&'||'eacute;polis</a></p>
    </td>
  </tr>
</table>
</body>
</html>
');

EXCEPTION
   WHEN OTHERS THEN
   htp.p('Error al consultar pel'||'&'||'iacute;cula: '||SQLCODE);
END;
/

