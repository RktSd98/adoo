CREATE OR REPLACE PROCEDURE ADM_VALIDA_BORRAR_CATEGORIA(p_cve_categoria NUMBER) IS

   /* Variables para la recuperacion de datos de la base de datos */
   v_descripcion    categorias.DESCRIPCION%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema    VARCHAR2(45);

BEGIN
   /* Obtenemos la fecha actual del sistema */
   v_fecha_sistema := obtener_fecha();

   /* Consultar registro */
   SELECT DESCRIPCION
     INTO v_descripcion
     FROM categorias
    WHERE CVE_CATEGORIA = p_cve_categoria;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top"><p class="textos">'||v_fecha_sistema||'</p></td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Eliminar Categor'||'&'||'iacute;a</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr>
    <td colspan="2" class="textos">Esta a punto de eliminar el siguiente registro:</td>
  </tr>
  <tr>
    <td class="textos">Clave: '||p_cve_categoria||'</td>
    <td class="textos">Descripcion: '||v_descripcion||'</td>
  </tr>
  <tr>
    <td colspan="2" align="center">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="textos">'||'&'||'iquest;Desea Continuar?</td>
  </tr>
  <tr>
    <td colspan="2" align="center">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <form name="form1" method="post" action="adm_borrar_categoria">
        <input type="hidden" name="p_cve_categoria" value="'||p_cve_categoria||'">
        <input type="submit" value="Aceptar">'||'&'||'nbsp;'||'&'||'nbsp;'||'&'||'nbsp;'||'&'||'nbsp;
		<input type="button" value="Cancelar" onClick="history.go(-1)">
      </form>
    </td>
  </tr>
</table>
</BODY>
</HTML>
   ');

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

