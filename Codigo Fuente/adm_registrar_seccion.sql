CREATE OR REPLACE PROCEDURE adm_registrar_seccion(p_cve_subcategoria NUMBER
                                                 ,p_cve_seccion      NUMBER
                                                 ,p_descripcion      VARCHAR2)AS

BEGIN
   /* Se insertan los datos en la tabla */
   INSERT INTO secciones(CVE_SUBCATEGORIA
                        ,CVE_SECCION
                        ,DESCRIPCION)
        VALUES(p_cve_subcategoria
              ,p_cve_seccion
              ,p_descripcion);

   /* Actualizaci�n de la base de datos */
   COMMIT;

   /* Presenta la consulta de eventos */
   owa_util.redirect_url('adm_mostrar_secciones?p_cve_subcategoria='||p_cve_subcategoria,FALSE);

/* Control de errores */
EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      UPDATE secciones
         SET DESCRIPCION      = p_descripcion
       WHERE CVE_SUBCATEGORIA = p_cve_subcategoria
         AND CVE_SECCION      = p_cve_seccion;

      /* Presenta la consulta de eventos */
      owa_util.redirect_url('adm_mostrar_secciones?p_cve_subcategoria='||p_cve_subcategoria,FALSE);

   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

