CREATE OR REPLACE PROCEDURE adm_clasificacion_add1 (p_tienda    VARCHAR2
                                                   ,p_num_local NUMBER) AS

   /* Variables para recuperar informacion de la base de datos */
   v_cve_categoria       categorias.CVE_CATEGORIA%TYPE;
   v_cat_descripcion     categorias.DESCRIPCION%TYPE;
   v_cve_subcategoria    subcategorias.CVE_SUBCATEGORIA%TYPE;
   v_sub_descripcion     subcategorias.DESCRIPCION%TYPE;
   v_cve_seccion         secciones.CVE_SECCION%TYPE;
   v_sec_descripcion     secciones.DESCRIPCION%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema  VARCHAR2(45);

   /* Declaracion de cursores */
   CURSOR categorias_cursor IS
         SELECT CVE_CATEGORIA
			   ,DESCRIPCION
           FROM categorias
       ORDER BY CVE_CATEGORIA;
BEGIN
   /* Mostrar Fecha */
   v_fecha_sistema := obtener_fecha;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador de Directorio</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top">
      <p class="textos">'||v_fecha_sistema||'</p>
    </td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Administrador de Directorio ('||p_tienda||')</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
  <tr>
    <td colspan="2" height="5">
      <div align="center">
      <FORM action="adm_clasificacion_add2" method="post" name="register_form">
        <table border="0" width="400" align="center">
          <tr>
            <td width="200" align="right" class="textos">Categor'||'&'||'iacute;a:</td>
            <td  width="200">
              <input type="hidden" name="p_tienda" value="'||p_tienda||'">
              <input type="hidden" name="p_num_local" value="'||p_num_local||'">
              <select name="p_categoria">
   ');

   /* Presentamos categorias */
   /* Abrir cursor */
   OPEN categorias_cursor;

   /* Recuperar el primer registro */
   FETCH categorias_cursor
    INTO v_cve_categoria
        ,v_cat_descripcion;

   /* Realiza mientras haya registros */
   WHILE (categorias_cursor%FOUND)
   LOOP
      /* Muestra resultados de la busqueda */
      htp.p('<option value="'||v_cve_categoria||'">'||v_cat_descripcion);

      /* Recuperar otro registro */
      FETCH categorias_cursor
       INTO v_cve_categoria
           ,v_cat_descripcion;
   END LOOP;

   /* Cerrar Cursor */
   CLOSE categorias_cursor;

   htp.p('
              </select>
            </td>
          </tr>
        </table>
          <table width="400" align="center">
            <tr>
              <td colspan="2" align="center">
                <input type="button" VALUE="Regresar" onClick="history.go(-1)">
                <input type="submit" value="Siguiente">
              </td>
            </tr>
          </table>
      </FORM>
      </div>
    </td>
  </tr>
</table>
</BODY>
</HTML>
   ');

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

