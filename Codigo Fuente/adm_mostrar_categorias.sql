CREATE OR REPLACE PROCEDURE ADM_MOSTRAR_CATEGORIAS AS

   /* Declaracion de variables para recuperar informacion de la Base de datos */
   v_cve_categoria categorias.CVE_CATEGORIA%TYPE;
   v_descripcion   categorias.DESCRIPCION%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema  VARCHAR2(45);

   /* Declaracion de cursores */
   CURSOR mostrar_categoria_cursor IS
      SELECT CVE_CATEGORIA
            ,DESCRIPCION
        FROM categorias
    ORDER BY CVE_CATEGORIA;
BEGIN

   /* Mostrar Fecha */
   v_fecha_sistema := obtener_fecha;

   /* Abrir cursor */
   OPEN mostrar_categoria_cursor;

   /* Recuperar el primer registro */
   FETCH mostrar_categoria_cursor
    INTO v_cve_categoria
        ,v_descripcion;

   /* C�digo HTML est�tico dentro del procedure */
   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top" class="textos">'||v_fecha_sistema||'</td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Administrador de Directorio</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
  <tr>
    <td valign="top">
      <form name ="agregar" method="get" action="../administrador/adm_categorias_add.htm">
        <input type="submit" value="Agregar Categor'||'&'||'iacute;a">
      </form>
    </td>
  </tr>
</table>

<table width="780" border="1" cellspacing="2" cellpadding="2">
  <tr>
    <td align="center" class="subtitulos3">Clave Categor'||'&'||'iacute;a</td>
    <td align="center" class="subtitulos3">Descripci'||'&'||'oacute;n</td>
    <td>'||'&'||'nbsp;</td>
    <td>'||'&'||'nbsp;</td>
    <td>'||'&'||'nbsp;</td>
  </tr>
');

   /* Realiza mientras haya registros */
   WHILE (mostrar_categoria_cursor%FOUND)
   LOOP
      /* Muestra resultados de la busqueda */
   htp.p('
  <tr>
    <td class="textos">'||v_cve_categoria||'</td>
    <td class="textos">'||v_descripcion||'</td>
    <td><a href="adm_mostrar_subcategorias?p_cve_categoria='||v_cve_categoria||'" class="links">Subcategorias</a></td>
    <td><a href="adm_recuperar_categoria?p_cve_categoria='||v_cve_categoria||'" class="links">Modificar</a></td>
    <td><a href="adm_valida_borrar_categoria?p_cve_categoria='||v_cve_categoria||'" class="links">Borrar</a></td>
  </tr>
');

      /* Recuperar otro registro */
      FETCH mostrar_categoria_cursor
       INTO v_cve_categoria
           ,v_descripcion;
   END LOOP;

   /* Cerrar Cursor */
   CLOSE mostrar_categoria_cursor;

   htp.p('
</table>
</BODY>
</HTML>
');

END;
/

