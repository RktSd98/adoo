CREATE OR REPLACE PROCEDURE adm_clasificacion_add3 (p_tienda        VARCHAR2
                                                   ,p_num_local     NUMBER
                                                   ,p_categoria     NUMBER
                                                   ,p_subcategoria  NUMBER) AS

   /* Variables para recuperar informacion de la base de datos */
   v_cat_descripcion   categorias.DESCRIPCION%TYPE;
   v_sub_descripcion   subcategorias.DESCRIPCION%TYPE;
   v_cve_seccion       secciones.CVE_SECCION%TYPE;
   v_sec_descripcion   secciones.DESCRIPCION%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema  VARCHAR2(45);

   /* Declaracion de cursores */
   CURSOR secciones_cursor IS
         SELECT CVE_SECCION
               ,DECODE(RTRIM(DESCRIPCION), NULL, 'Ninguna', RTRIM(DESCRIPCION))
           FROM secciones
          WHERE CVE_SUBCATEGORIA = p_subcategoria
       ORDER BY CVE_SUBCATEGORIA;
BEGIN
   /* Mostrar Fecha */
   v_fecha_sistema := obtener_fecha;

   /* Recuperamos el nombre de la categoria y subcategoria */
   SELECT DESCRIPCION
     INTO v_cat_descripcion
     FROM categorias
    WHERE CVE_CATEGORIA = p_categoria;

   SELECT DECODE(RTRIM(DESCRIPCION), NULL, 'Ninguna', RTRIM(DESCRIPCION))
     INTO v_sub_descripcion
     FROM subcategorias
    WHERE CVE_SUBCATEGORIA = p_subcategoria;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador de Directorio</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../../siicchtm/styles/estilos.css">
<BODY background="../../siicchtm/images/Fondo_adm.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../../siicchtm/images/powered_redHat.gif" width="85" height="32"></td>
    <td width="190" align="left" valign="top">
      <p class="textos">'||v_fecha_sistema||'</p>
    </td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones" bgcolor="#FFFFFF">Administrador de Directorio ('||p_tienda||')</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../../siicchtm/images/lineah.gif" width="780" height="2"></td>
  </tr>
  <tr>
    <td colspan="2" height="5">
      <div align="center">
      <FORM action="adm_registrar_clasificacion" method="post" name="register_form">
        <table border="0" width="400" align="center">
          <tr>
            <td width="200" align="right" class="textos">Categor'||'&'||'iacute;a:</td>
            <td width="200" class="textos">'||v_cat_descripcion||'</td>
          </tr>
          <tr>
            <td width="200" align="right" class="textos">Subcategor'||'&'||'iacute;a:</td>
            <td width="200" class="textos">'||v_sub_descripcion||'</td>
          </tr>
          <tr>
            <td width="200" align="right" class="textos">Secci'||'&'||'oacute;n:</td>
            <td  width="200">
              <input type="hidden" name="p_tienda" value="'||p_tienda||'">
              <input type="hidden" name="p_num_local" value="'||p_num_local||'">
              <input type="hidden" name="p_categoria" value="'||p_categoria||'">
              <input type="hidden" name="p_subcategoria" value="'||p_subcategoria||'">
              <select name="p_seccion">
                <option value="0">Ninguna
   ');

   /* Presentamos categorias */
   /* Abrir cursor */
   OPEN secciones_cursor;

   /* Recuperar el primer registro */
   FETCH secciones_cursor
    INTO v_cve_seccion
        ,v_sec_descripcion;

   /* Realiza mientras haya registros */
   WHILE (secciones_cursor%FOUND)
   LOOP
      /* Presenta solo si la cve_seccion es mayor a cero */
      IF(v_cve_seccion <> 0) THEN
         /* Muestra resultados de la busqueda */
         htp.p('<option value="'||v_cve_seccion||'">'||v_sec_descripcion);
      END IF;

      /* Recuperar otro registro */
      FETCH secciones_cursor
       INTO v_cve_seccion
           ,v_sec_descripcion;
   END LOOP;

   /* Cerrar Cursor */
   CLOSE secciones_cursor;

   htp.p('
              </select>
            </td>
          </tr>
        </table>
          <table width="400" align="center">
            <tr>
              <td colspan="2" align="center">
                <input type="button" VALUE="Regresar" onClick="history.go(-1)">
                <input type="submit" value="Siguiente">
              </td>
            </tr>
          </table>
      </FORM>
      </div>
    </td>
  </tr>
</table>
</BODY>
</HTML>
   ');

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/
