CREATE OR REPLACE PROCEDURE adm_registrar_categoria(p_cve_categoria NUMBER
                                                   ,p_descripcion   VARCHAR2)AS

BEGIN
   /* Se insertan los datos en la tabla */
   INSERT INTO categorias(CVE_CATEGORIA
                         ,DESCRIPCION)
        VALUES(p_cve_categoria
              ,p_descripcion);

   /* Actualizaci�n de la base de datos */
   COMMIT;

   /* Presenta la consulta de eventos */
   owa_util.redirect_url('adm_mostrar_categorias',FALSE);

/* Control de errores */
EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      UPDATE categorias
        SET  DESCRIPCION      = p_descripcion
       WHERE CVE_CATEGORIA = p_cve_categoria;

      /* Presenta la consulta de eventos */
      owa_util.redirect_url('adm_mostrar_categorias',FALSE);

   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

