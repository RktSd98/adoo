CREATE OR REPLACE PROCEDURE adm_mostrar_locales AS

   /* Declaracion de variables */
   v_num_local             locales.num_local%TYPE;
   v_tienda                locales.tienda%TYPE;
   v_fecha_sistema         VARCHAR2(45);

   /* Variables auxiliares */
   v_inicio         NUMBER(3);
   v_contador       NUMBER(3) := 0;
   v_anterior       NUMBER(3);
   v_siguiente      NUMBER(3);
   v_max_num_local  NUMBER(3);
   v_min_num_local  NUMBER(3);
   v_aux            NUMBER(3);

   /* Declaracion de cursores */
   CURSOR mostrar_locales_cursor IS
          SELECT locales.num_local
                ,locales.tienda
            FROM locales
          ORDER BY num_local;
BEGIN

   /* Mostrar Fecha */
    v_fecha_sistema := obtener_fecha;

   /* Indentifica si se selecciono PREVIO o SIGUIENTE */
--   IF(p_estado = 0) THEN
--      v_inicio := p_local_prev;
--
--     ELSE
--      v_inicio := p_local_next;
--   END IF;

   /* Obtenemos el valor m�ximo de num_local */
--   SELECT MAX(num_local)
--     INTO v_max_num_local
--     FROM locales;

   /* Obtenemos el valor m�nimo de num_local
   SELECT MIN(num_local)
      INTO v_min_num_local
      FROM locales;

   /* Abrir cursor */
   OPEN mostrar_locales_cursor;

   /* Recuperar el primer registro */
   FETCH mostrar_locales_cursor
    INTO v_num_local
        ,v_tienda ;

   /* Variable Auxiliar para guardar primer num_local consultado */
--    v_aux:= p_local_prev;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top" class="textos">'||v_fecha_sistema||'</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="secciones">Administrador de Locales</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
  <tr>
    <td valign="top">
      <form name ="agregar" method="get" action="../administrador/adm_locales_add.htm">
        <input type="submit" value="Agregar Local">
      </form>
    </td>
  </tr>
</table>

<table width="780" border="1" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100" align="center" class="subtitulos3">Num. Local</td>
    <td align="center" class="subtitulos3">Tienda</td>
    <td width="80">'||'&'||'nbsp;</td>
    <td width="80">'||'&'||'nbsp;</td>
    <td width="80">'||'&'||'nbsp;</td>
  </tr>
');

   /* Realiza mientras haya registros */
   WHILE (mostrar_locales_cursor%FOUND)
   LOOP
      /* Muestra resultados de la busqueda */
   htp.p('
  <tr>
    <td width="100" class="textos">'||v_num_local||'</td>
    <td class="textos">'||v_tienda||'</td>
    <td width="80"><a href="adm_mostrar_clasificacion?p_num_local='||v_num_local||'&'||'p_tienda='||v_tienda||'" class="links">Clasificar</a></td>
    <td width="80"><a href="adm_recuperar_local?p_num_local='||v_num_local||'" class="links">Modificar</a></td>
    <td width="80"><a href="adm_valida_borrar_local?p_num_local='||v_num_local||'" class="links">Borrar</a></td>
  </tr>
');

      /* Recuperar otro registro */
      FETCH mostrar_locales_cursor
       INTO  v_num_local
            ,v_tienda ;
   END LOOP;

   /* Cerrar Cursor */
   CLOSE mostrar_locales_cursor;

   htp.p('
</table>
</BODY>
</HTML>
');


END;
/

