CREATE OR REPLACE PROCEDURE adm_recuperar_local(p_num_local NUMBER) AS

   /* Variables auxiliares */
   v_num_local       locales.num_local%TYPE;
   v_tienda          locales.tienda%TYPE;
   v_descripcion     locales.descripcion%TYPE;
   v_palabras        locales.palabras%TYPE;
   v_gerente         locales.gerente%TYPE;
   v_telefono_1      locales.telefono_1%TYPE;
   v_telefono_2      locales.telefono_2%TYPE;
   v_fax             locales.fax%TYPE;
   v_url             locales.url%TYPE;
   v_email           locales.email%TYPE;
   v_piso            locales.piso%TYPE;
   v_coordenadas     locales.coordenadas%TYPE;
   v_foto_fachada    locales.foto_fachada%TYPE;
   v_foto_promocion  locales.foto_promocion%TYPE;
   v_desc_prom_1     locales.desc_prom_1%TYPE;
   v_desc_prom_2     locales.desc_prom_2%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema   VARCHAR2(50);

BEGIN

   /* Consultar registro del local */
   SELECT locales.num_local
         ,locales.tienda
         ,locales.descripcion
         ,locales.palabras
         ,locales.gerente
         ,locales.telefono_1
         ,locales.telefono_2
         ,locales.fax
         ,locales.url
         ,locales.email
         ,locales.piso
         ,locales.coordenadas
         ,locales.foto_fachada
         ,locales.foto_promocion
         ,locales.desc_prom_1
         ,locales.desc_prom_2
     INTO v_num_local
         ,v_tienda
         ,v_descripcion
         ,v_palabras
         ,v_gerente
         ,v_telefono_1
         ,v_telefono_2
         ,v_fax
         ,v_url
         ,v_email
         ,v_piso
         ,v_coordenadas
         ,v_foto_fachada
         ,v_foto_promocion
         ,v_desc_prom_1
         ,v_desc_prom_2
     FROM locales
    WHERE num_local= p_num_local;

   /* Recuperar fecha del sistema */
   v_fecha_sistema := obtener_fecha;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<SCRIPT language=JavaScript>
<!--
function checkrequired(which){
   // Validar Nombre de archivo
   if(which.p_archivo.value == "") {
      if(which.p_archivo_aux.value == "") {
         alert("Ingrese nombre del archivo");
         which.p_archivo_aux.focus();
         return false;
      }
   }

   //Valida numero de local
   if(which.p_num_local.value==""){
      alert("Ingrese numero de local");
      which.p_num_local.focus();
      return false;
   }else {
      if (isNaN(which.p_num_local.value)){
        alert("El numero del local solo deben ser numeros");
        which.p_num_local.focus();
        return false;
      }else {
         if(eval(which.p_num_local.value)<=0){
            alert("El numero del local debe ser mayor a cero");
            which.p_num_local.focus();
            return false;
         }
      }
   }

   //Valida nombre tienda
   if(which.p_tienda.value==""){
      alert("Ingrese nombre de la tienda");
      which.p_tienda.focus();
      return false;
   }

   //Valida palabras
   if(which.p_palabras.value==""){
      alert("Ingrese palabras");
      which.p_palabras.focus(); 
      return false;
   }

   //Retorna True si pasa todas las validaciones
   return true;
}
//-->
</SCRIPT>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top">
      <p class="textos">'||v_fecha_sistema||'</p>
    </td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Modificar Local</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <FORM action="/servlet/uploadLocal" ENCTYPE="multipart/form-data" method="post" name="register_form" onsubmit="return checkrequired(this)">
        <table width="600">
          <tr>
            <td width="150" align="right" class="textos">Foto fachada:</td>
            <td width="450" colspan="3">
              <input type="hidden" name="p_path" value="/u01/app/oracle/product/901/Apache/Apache/htdocs/siicchtm/01directorio/01locales">
              <input type="hidden" name="p_id" value="'||v_num_local||'">
              <input type="text" name="p_archivo_aux" value="'||v_foto_fachada||'">
              <input type="FILE" name="p_archivo" readonly>
            </td>
          </tr>
          <tr>
              <td width="150" align="right" class="textos">N'||'&'||'uacute;mero de Local:</td>
              <td width="150"><input type="text" name="p_num_local" value="'||v_num_local||'" maxlength="3"></td>
              <td width="150" align="right" class="textos">Nombre de la Tienda:</td>
              <td width="150"><input type="text" name="p_tienda" value="'||v_tienda||'" maxlength="50"></td>
          </tr>
          <tr>
              <td width="150" align="right" class="textos">Nombre del Gerente:</td>
              <td width="150"><input type="text" name="p_gerente" value="'||v_gerente||'" maxlength="50"></td>
              <td width="150" align="right" class="textos">Piso:</td>
              <td width="150"><input type="text" name="p_piso" value="'||v_piso||'" maxlength="1"></td>
          </tr>
          <tr>
              <td width="150" align="right" class="textos">Tel'||'&'||'eacute;fono 1:</td>
              <td width="150"><input type="text" name="p_telefono_1" value="'||v_telefono_1||'" maxlength="15"></td>
              <td width="150" align="right" class="textos">Tel'||'&'||'eacute;fono 2:</td>
              <td width="150"><input type="text" name="p_telefono_2" value="'||v_telefono_2||'" maxlength="15"></td>
          </tr>
          <tr>
              <td width="150" align="right" class="textos">Fax:</td>
              <td width="150"><input type="text" name="p_fax" value="'||v_fax||'" maxlength="15"></td>
              <td width="150" align="right" class="textos">Coordenadas:</td>
              <td width="150"><input type="text" name="p_coordenadas" value="'||v_coordenadas||'" maxlength="10" ></td>
          </tr>
          <tr>
              <td width="150" align="right" class="textos">Url:</td>
              <td width="150"><input type="text" name="p_url" value="'||v_url||'" maxlength="50"></td>
              <td width="150" align="right" class="textos">Correo Electr'||'&'||'oacute;nico:</td>
              <td width="150"><input type="text" name="p_email" value="'||v_email||'" maxlength="40"></td>
          </tr>
          <tr>
              <td width="150" align="right" class="textos">Palabras:</td>
              <td colspan="3" width="450" align="center"><textarea cols="50" name="p_palabras" rows="7">'||v_palabras||'</textarea></td>
          </tr>
          <tr>
              <td width="150" align="right" class="textos">Descripci'||'&'||'oacute;n:</td>
              <td colspan="3" width="450" align="center"><textarea cols="50" name="p_descripcion" rows="4">'||v_descripcion||'</textarea></td>
          </tr>
<!-- AAH podria utilizarse, si las promociones se hicieran dinamicas.
          <tr>
              <td width="150" align="right" class="textos">Desc. promoci'||'&'||'oacute;n 1:</td>
              <td colspan="3" width="450" align="center"><textarea cols="50" name="p_desc_prom_1" rows="4"></textarea></td>
          </tr>
          <tr>
              <td width="150" align="right" class="textos">Desc. promoci'||'&'||'oacute;n 2:</td>
              <td colspan="3" width="450" align="center"><textarea cols="50" name="p_desc_prom_2" rows="4"></textarea></td>
          </tr>
-->
        </table>
        <table width="400" align="center">
          <tr>
            <td colspan="2" align="center">
             <input type="submit" value="Enviar">
             <input type="button" VALUE="Cancelar" onClick="history.go(-1)">
            </td>
          </tr>
        </table>
      </FORM>
    </td>
  </tr>
</table>
</BODY>
</HTML>
   ');

END;
/


