CREATE OR REPLACE PROCEDURE ADM_BORRAR_SUBCATEGORIA(p_cve_categoria    NUMBER
                                                   ,p_cve_subcategoria NUMBER) IS

   /* Variables para la recuperacion de informacion de la base de datos */
   v_num_secciones   NUMBER(6);
   v_num_registros   NUMBER(6);

   /* Variables auxiliares */
   v_fecha_sistema VARCHAR2(45);
BEGIN
   /* Revisar que no tenga secciones o que no haya locales asociados con esta subcategoria */
   SELECT count(*)
     INTO v_num_secciones
     FROM secciones
    WHERE CVE_SUBCATEGORIA = p_cve_subcategoria;

   SELECT count(*)
     INTO v_num_registros
     FROM directorio
    WHERE CVE_SUBCATEGORIA = p_cve_subcategoria;

   IF(v_num_secciones = 0 AND v_num_registros = 0) THEN
      /* Borra registro de la tabla */
      DELETE FROM subcategorias
       WHERE CVE_SUBCATEGORIA = p_cve_subcategoria;

      COMMIT;

     /* Presenta consulta */
      owa_util.redirect_url('adm_mostrar_subcategorias?p_cve_categoria='||p_cve_categoria,FALSE);
   ELSE
      htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top"><p class="textos">'||v_fecha_sistema||'</p></td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Eliminar Subcategor'||'&'||'iacute;a</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr>
    <td colspan="2" class="textos">
      No se pudo borrar la subcategor'||'&'||'iacute;a. Puede que tenga secciones o locales asociados a esta.<br>
      Para borrarla, asegurese que no haya ninguna secci'||'&'||'oacute;n o local asociado a esta subcategor'||'&'||'iacute;a.
    </td>
  </tr>
  <tr>
    <td colspan="2">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <form name="form1" method="post" action="adm_mostrar_subcategorias">
		<input type="hidden" name="p_cve_categoria" value="'||p_cve_categoria||'">
		<input type="submit" value="Regresar">
      </form>
    </td>
  </tr>
</table>
</BODY>
</HTML>
      ');
   END IF;

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

