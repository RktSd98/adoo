CREATE OR REPLACE PROCEDURE adm_registrar_clasificacion(p_tienda      VARCHAR2
                                                       ,p_num_local    NUMBER
                                                       ,p_categoria    NUMBER
                                                       ,p_subcategoria NUMBER
                                                       ,p_seccion      NUMBER)AS

BEGIN
   /* Se insertan los datos en la tabla */
   INSERT INTO directorio(NUM_LOCAL
                         ,CVE_CATEGORIA
                         ,CVE_SUBCATEGORIA
                         ,CVE_SECCION)
        VALUES(p_num_local
              ,p_categoria
              ,p_subcategoria
              ,p_seccion);

   /* Actualizaci�n de la base de datos */
   COMMIT;

   /* Presenta la consulta de eventos */
   owa_util.redirect_url('adm_mostrar_clasificacion?p_tienda='||p_tienda||'&'||'p_num_local='||p_num_local,FALSE);

/* Control de errores */
EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      ROLLBACK;

      /* Presenta la consulta de eventos */
      owa_util.redirect_url('adm_mostrar_clasificacion?p_tienda='||p_tienda||'&'||'p_num_local='||p_num_local,FALSE);

   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/

