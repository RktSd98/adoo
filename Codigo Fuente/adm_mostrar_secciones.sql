CREATE OR REPLACE PROCEDURE ADM_MOSTRAR_SECCIONES(p_cve_subcategoria NUMBER)AS

   /* Declaracion de variables para recuperar informacion de la Base de datos */
   v_cve_seccion      secciones.CVE_SECCION%TYPE;
   v_descripcion      secciones.DESCRIPCION%TYPE;
   v_cve_categoria    categorias.DESCRIPCION%TYPE;
   v_sub_descripcion  subcategorias.DESCRIPCION%TYPE;

   /* Variables auxiliares */
   v_fecha_sistema  VARCHAR2(45);

   /* Declaracion de cursores */
   CURSOR mostrar_seccion_cursor IS
      SELECT CVE_SECCION
            ,DESCRIPCION
        FROM secciones
       WHERE CVE_SUBCATEGORIA = p_cve_subcategoria
    ORDER BY CVE_SECCION;

BEGIN

   /* Mostrar Fecha */
   v_fecha_sistema := obtener_fecha;

   /* Recuperamos descripcion de subcategoria */
   SELECT CVE_CATEGORIA
         ,DESCRIPCION
     INTO v_cve_categoria
         ,v_sub_descripcion
     FROM subcategorias
    WHERE CVE_SUBCATEGORIA = p_cve_subcategoria;

   /* Abrir cursor */
   OPEN mostrar_seccion_cursor;

   /* Recuperar el primer registro */
   FETCH mostrar_seccion_cursor
    INTO v_cve_seccion
        ,v_descripcion;

   /* C�digo HTML est�tico dentro del procedure */
   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top" class="textos">'||v_fecha_sistema||'</td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Administrador de Directorio ('||p_cve_subcategoria||'-'||v_sub_descripcion||')</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
  <tr>
    <td valign="top">
      <form name ="agregar" method="get" action="../administrador/adm_secciones_add.htm">
        <input type="hidden" name="p_cve_subcategoria" value="'||p_cve_subcategoria||'">
        <input type="submit" value="Agregar Secci'||'&'||'oacute;n">
      </form>
    </td>
    <td>
      <form name ="regresar" method="get" action="adm_mostrar_subcategorias">
        <input type="hidden" name="p_cve_categoria" value="'||v_cve_categoria||'">
        <input type="submit" value="Regresar">
      </form>
    </td>
  </tr>
</table>

<table width="780" border="1" cellspacing="2" cellpadding="2">
  <tr>
    <td align="center" class="subtitulos3">Clave Secci'||'&'||'oacute;n</td>
    <td align="center" class="subtitulos3">Descripci'||'&'||'oacute;n</td>
    <td>'||'&'||'nbsp;</td>
    <td>'||'&'||'nbsp;</td>
  </tr>
');

   /* Realiza mientras haya registros */
   WHILE (mostrar_seccion_cursor%FOUND)
   LOOP
      /* Muestra resultados de la busqueda */
   htp.p('
  <tr>
    <td class="textos">'||v_cve_seccion||'</td>
    <td class="textos">'||v_descripcion||'</td>
    <td><a href="adm_recuperar_seccion?p_cve_seccion='||v_cve_seccion||'" class="links">Modificar</a></td>
    <td><a href="adm_valida_borrar_seccion?p_cve_seccion='||v_cve_seccion||'" class="links">Borrar</a></td>
  </tr>
');

      /* Recuperar otro registro */
      FETCH mostrar_seccion_cursor
       INTO v_cve_seccion
           ,v_descripcion;
   END LOOP;

   /* Cerrar Cursor */
   CLOSE mostrar_seccion_cursor;

   htp.p('
</table>
</BODY>
</HTML>
');

END;
/

