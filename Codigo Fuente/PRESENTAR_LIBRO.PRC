CREATE OR REPLACE PROCEDURE presentar_libro(l_id NUMBER) AS

   /* Variables para la recuperacion de datos de la base de datos */
   v_archivo          libros.ARCHIVO%TYPE;
   v_titulo           libros.TITULO%TYPE;
   v_autor            libros.AUTOR%TYPE;
   v_url_destino      libros.URL_DESTINO%TYPE;
   v_fecha_vig_inicio libros.FECHA_VIG_INICIO%TYPE;
   v_fecha_vig_fin    libros.FECHA_VIG_FIN%TYPE;
     
   BEGIN

   /* Consultar registro de la base de datos */
   SELECT ARCHIVO
         ,TITULO
         ,AUTOR
         ,URL_DESTINO
         ,TO_CHAR(FECHA_VIG_INICIO,'DD/MM/YYYY')
         ,TO_CHAR(FECHA_VIG_FIN,'DD/MM/YYYY')
     INTO v_archivo
         ,v_titulo
         ,v_autor
         ,v_url_destino
         ,v_fecha_vig_inicio
         ,v_fecha_vig_fin
     FROM libros
    WHERE id= l_id;

htp.p('
<html>
<head>
<title>SIICC IPN</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language="JavaScript1.2">
function winclose(){
   window.close()
   self.close()
}
</script>
<link rel="stylesheet" href="../00styles/estilos.css">
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#122966" link="#da004e">
<table width="450" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="190">'||'&'||'nbsp;</td>
    <td width="260"><img src="../00superior/00imagenes1/00_menu_logo2.gif" width="87" height="108" align="top" name="Image1"></td>
  </tr>
  <tr>
    <td width="190" class="textos">
      <p align="center">
   ');

   IF(v_url_destino IS NULL) THEN
      htp.p('<img src="../../siicchtm/entretent/imagenes/'||v_archivo||'" width="190" height="199">');
   ELSE
      htp.p('<a href="'||v_url_destino||'" onClick="window.close()" target="_blank"><img src="../../siicchtm/entretent/imagenes/'||v_archivo||'" width="190" height="199" border="0"></a>');
   END IF;

   htp.p('
     '||v_titulo||'</p>
      <p align="center" class="textos"><b>Interprete</b>:'||v_autor||'</p>
     ');

   
   htp.p('
      </td>
    <td width="260" class="normal" valign="top">
      <p>'||'&'||'nbsp;</p>
      
      <p class="normal" align="right">
	    <a href="http://anafre.escom.ipn.mx" onClick="window.close()" target="_blank" class="links">Ir
        a SIICC</a></p>
    </td>
  </tr>
</table>
</body>
</html>
');

EXCEPTION
   WHEN OTHERS THEN
   htp.p('Error al consultar libro: '||SQLCODE);
END;
/

