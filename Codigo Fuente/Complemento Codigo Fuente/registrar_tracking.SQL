CREATE OR REPLACE FUNCTION registrar_tracking (p_num_tracking      VARCHAR2
                                              ,p_valor             VARCHAR2
                                              ,p_fecha             DATE) RETURN VARCHAR2 AS


   /* Declaraacion de variables */
 
BEGIN

   /* Determinamos cual sera la condicion a tomar */
   
      INSERT INTO tracking
        VALUES(p_num_tracking
              ,p_valor
              ,p_fecha);

   COMMIT;
  
   
   RETURN v_tracking;

EXCEPTION
   WHEN OTHERS THEN
      v_fecha_sistema := 'Error al obtener fecha del sistema:'||SQLCODE;
      RETURN v_fecha_sistema;
END;
/
