CREATE OR REPLACE PROCEDURE ADM_BORRAR_PRODUCTO (p_num_producto NUMBER) IS

BEGIN
   /* Borra registro de la tabla PRODUCTOS*/
   DELETE FROM productos
    WHERE num_producto = p_num_producto;

   COMMIT;

   /* Presenta consulta */
   owa_util.redirect_url('adm_mostrar_productos',FALSE);

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/
