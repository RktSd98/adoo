CREATE OR REPLACE PROCEDURE mostrar_tracking(p_num_local  NUMBER) AS

   /* Declaracion de variables */
   v_fecha            tracking.FECHA%TYPE;
   v_num_local        tracking.NUM_LOCAL%TYPE;
   v_num_producto     tracking.NUM_PRODUCTO%TYPE;
   v_num_tracking     tracking.NUM_TRACKING%TYPE;
   v_valor_tracking   tracking.VALOR%TYPE;
   v_fecha_sistema         VARCHAR2(45);

   /* Variables auxiliares */
   v_inicio         NUMBER(3);
   v_contador       NUMBER(3) := 0;
   v_anterior       NUMBER(3);
   v_siguiente      NUMBER(3);
   v_max_num_produc NUMBER(3);
   v_min_num_produc NUMBER(3);
   v_aux            NUMBER(3);
   p_valor          VARCHAR2(1000);

   /* Declaracion de cursores */
   CURSOR mostrar_tracking_cursor IS
          SELECT tracking.FECHA
                ,tracking.NUM_LOCAL
				,tracking.NUM_PRODUCTO
				,tracking.NUM_TRACKING
				,tracking.VALOR
            FROM tracking
		   WHERE num_local = p_num_local
          ORDER BY num_tracking;
BEGIN

   /* Mostrar Fecha */
    v_fecha_sistema := obtener_fecha;

   /* Abrir cursor */
   OPEN mostrar_tracking_cursor;

   /* Recuperar el primer registro */
   FETCH mostrar_tracking_cursor
    INTO v_fecha
        ,v_num_local
		,v_num_producto
		,v_num_tracking
		,v_valor_tracking ;

   /* Variable Auxiliar para guardar primer num_local consultado */
--    v_aux:= p_local_prev;

   htp.p('
<HTML>
<HEAD>
<TITLE>Locatario</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">

<table border="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top" class="textos">'||v_fecha_sistema||'</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="secciones">REPORTE</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
</table>

<table width="780" border="1" cellspacing="2" cellpadding="2">
  <tr>
    <td width="120" align="center" class="subtitulos3">Fecha de Operacion</td>
    <td width="30" align="center" class="subtitulos3">No. de Local</td>
	<td width="100" align="center" class="subtitulos3">Producto</td>
	<td width="100" align="center" class="subtitulos3">Origen</td>
	<td width="100" align="center" class="subtitulos3">Seguimiento</td>
    <td width="80">'||'&'||'nbsp;</td>
    <td width="80">'||'&'||'nbsp;</td>
  </tr>
');

   /* Realiza mientras haya registros */
   WHILE (mostrar_tracking_cursor%FOUND)
   LOOP
      /* Muestra resultados de la busqueda */
   htp.p('
  <tr>
    <td width="100" class="textos">'||v_fecha||'</td>
    <td class="textos">'||v_num_local||'</td>
	<td class="textos">'||v_num_producto||'</td>
    <td class="textos">'||v_num_tracking||'</td>
	    <td class="textos">'||v_valor_tracking||'</td>
    <td width="80"><a href="adm_traducir_tracking?p_valor_tracking='||v_valor_tracking||'" class="links">Interpretar</a></td>
    <td width="80"><a href="valida_borrar_tracking?p_valor='||v_valor_tracking||''||'&'||'p_num_local='||p_num_local||'" class="links">Borrar</a></td>
  </tr>
');

      /* Recuperar otro registro */
    FETCH mostrar_tracking_cursor
    INTO v_fecha
        ,v_num_local
		,v_num_producto
		,v_num_tracking
		,v_valor_tracking ;

   END LOOP;

   /* Cerrar Cursor */
   CLOSE mostrar_tracking_cursor;

   htp.p('
</table>
</BODY>
</HTML>
');


END;
/
