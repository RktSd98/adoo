CREATE OR REPLACE PROCEDURE VALIDA_BORRAR_TRACKING (p_valor VARCHAR2
                                                    ,p_num_local NUMBER) AS

   /* Variables para la recuperacion de datos de la base de datos */
   v_valor     tracking.VALOR%TYPE;
   v_fecha_sistema      VARCHAR2(45);
     
BEGIN
   
   /* Obtenemos la fecha actual del sistema */
   v_fecha_sistema := obtener_fecha();
   

   /* Consultar registro de la tala de compras temporales*/
   SELECT  VALOR          
        
     INTO v_valor
        
     FROM  tracking tr
    WHERE p_valor = tr.VALOR;

   htp.p('
<HTML>
<HEAD>
<TITLE>Administrador</TITLE>
<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
<STYLE type=text/css></STYLE>
</HEAD>
<link rel="stylesheet" href="../00styles/estilos.css">
<BODY background="../00generales/00_back.gif" bgColor="#ffffff" leftMargin="0"  link="#da004e" text="#122966" topMargin="0" vLink="#da004e" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr align="left" valign="top">
    <td width="590"><img src="../00generales/logo.gif" width="235" height="56"></td>
    <td width="190" align="left" valign="top"><p class="textos">'||v_fecha_sistema||'</p></td>
  </tr>
  <tr>
    <td colspan = "2" align="center" class="secciones">Eliminar producto</td>
  </tr>
  <tr>
    <td colspan="2" height="5"><img src="../00generales/00_linea_h.gif" width="780" height="5"></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tr>
    <td colspan="2" class="textos">Esta a punto de quitar de su lista de compras el siguiente producto:</td>
  </tr>
  <tr>
    <td colspan="2" class="textos">Seguimiento:  '||v_valor||'</td>
  </tr>
  <tr>
    <td colspan="2" align="center">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="textos">'||'&'||'iquest;Desea Continuar?</td>
  </tr>
  <tr>
    <td colspan="2" align="center">'||'&'||'nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <form name="form1" method="post" action="borrar_tracking">
        <input type="hidden" name="p_valor" value="'||v_valor||'">
		<input type="hidden" name="p_num_local" value="'||p_num_local||'">

        <input type="submit" value="Aceptar">'||'&'||'nbsp;'||'&'||'nbsp;'||'&'||'nbsp;'||'&'||'nbsp;
		<input type="button" VALUE="Cancelar" onClick="history.go(-1)">
      </form>
    </td>
  </tr>
</table>
</BODY>
</HTML>
   ');

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('ERROR en procedimiento: '||sqlcode);

END;
/
