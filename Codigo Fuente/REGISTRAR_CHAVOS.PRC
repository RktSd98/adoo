CREATE OR REPLACE PROCEDURE registrar_chavos (
                            p_nombre           VARCHAR2
                           ,p_ape_paterno      VARCHAR2
                           ,p_ape_materno      VARCHAR2
                           ,p_sexo             VARCHAR2
                           ,p_fecha_nacimiento VARCHAR2
                           ,p_escuela          VARCHAR2
                           ,p_nivel            VARCHAR2
                           ,p_grado            NUMBER
                           ,p_caricatura       VARCHAR2
                           ,p_colonia          VARCHAR2
                           ,p_cod_postal       VARCHAR2
                           ,p_email            VARCHAR2
                           ,p_contacto         NUMBER
                           ,p_promocion        VARCHAR2
                           ,p_contrasena       VARCHAR2
                           ,p_conf_contrasena  VARCHAR2)AS


BEGIN

   /* Inserta registro en la tabla chavos */
   INSERT INTO chavos
   (nombre, ape_paterno, ape_materno, cod_postal, email, contrasenia
   ,fecha_nacimiento, sexo, escuela, nivel_escolar, grado, caricatura
   ,colonia,contacto,fecha_registro, cve_promocion)
   VALUES(p_nombre
         ,p_ape_paterno
         ,p_ape_materno
         ,p_cod_postal
         ,p_email
         ,p_contrasena
         ,TO_DATE(p_fecha_nacimiento, 'DD/MM/YYYY')
         ,p_sexo
         ,p_escuela
         ,p_nivel
         ,p_grado
         ,p_caricatura
         ,p_colonia
         ,p_contacto
         ,SYSDATE
         ,p_promocion);
   COMMIT;

    /* Presenta los Juegos */
    owa_util.redirect_url('../../siicchtm/03Chavos/pag_chavos.htm',FALSE);

EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      /* Actualiza registro, ya que, ya existia */
      UPDATE chavos
         SET nombre           = p_nombre
            ,ape_paterno      = p_ape_paterno
            ,ape_materno      = p_ape_materno
            ,cod_postal       = p_cod_postal
            ,contrasenia      = p_contrasena
            ,fecha_nacimiento = TO_DATE(p_fecha_nacimiento, 'DD/MM/YYYY')
            ,sexo             = p_sexo
            ,escuela          = p_escuela
            ,nivel_escolar    = p_nivel
            ,grado            = p_grado
            ,caricatura       = p_caricatura
            ,colonia          = p_colonia
            ,contacto         = p_contacto
            ,cve_promocion    = p_promocion
       WHERE email      = p_email;

      COMMIT;

       /* Presenta los Juegos */
       owa_util.redirect_url('../../siicchtm/03Chavos/pag_chavos.htm',FALSE);

   WHEN OTHERS THEN
      ROLLBACK;
      htp.p('Error en procedimiento: '||SQLCODE);
END;
/
