CREATE OR REPLACE PROCEDURE presentar_login_locatarios(p_mensaje IN OUT VARCHAR2) AS                  
                                                                                
   /* Declaracion de variables */                                               
   v_fecha_sistema  VARCHAR2(45);                                               
                                                                                
BEGIN                                                                           
                                                                                
htp.p('                                                                         
<HTML><title>SIICC IPN</title>                                                  
<link rel="stylesheet" href="../00styles/estilos.css">                          
<BODY leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="" background="/siicchtm/03Chavos/images/Fondo_Chavos.gif" text="#122966" link="#da004e" vlink="#da004e">                                                                     
<table width="780" border="0" cellspacing="0" cellpadding="0">                  
    <tr>                                                                        
      <td width="5">'||'&'||'nbsp;</td>                                         
      <td width="190" valign="top" class="textos">                              
      <p>Por favor escriba su correo electr'||'&'||'oacute;nico y su contrase'||'&'||'ntilde;a para ver el reporte.</p>                                                                              
      </td>                                                                     
      <td width="1" background="/siicchtm/images/linea_v.gif">'||'&'||'nbsp;</td>
      <td width="300" valign="top">                                             
      <table width="100%" >                                                     
       <tr>                                                                     
        <td>                                                                    
          <form name="login_form" method="get" action="validar_login_locatarios">           
          <table>                                                               
           <tr>                                                                 
            <td class="textos"><p><font color="#DA004E"><B>Correo Electr'||'&'||'oacute;nico</B></font></p></td>                                               
            <td><input type="text" name="p_email"></td>                         
           </tr>                                                                
           <tr>                                                                 
            <td class="textos"><p><font color="#DA004E"><B>Contrase'||'&'||'ntilde;a</B></font></p></td>                                                        
            <td><input type="password" name="p_contrasenia"></td>                
           </tr>
		   <tr>                                                                 
            <td class="textos"><p><font color="#DA004E"><B>No. Local</B></font></p></td>                                                        
            <td><input type="text" name="p_num_local"></td>                
           </tr>                                                                
           <tr>                                                                 
            <td><p>'||'&'||'nbsp;</p></td>                                      
            <td>                                                                
             <p><img src="/siicchtm/images/shim.gif" width="45" height="20" align="middle" border="0">                                                          
             <input type="submit" value="Enviar"></p>                           
            </td>                                                               
           </tr>                                                                
          </table>                                                              
          </form>                                                               
        </td>                                                                   
       </tr>                                                                    
       <tr>                                                                     
        <td>                                                                    
              <p>'||'&'||'nbsp;</p>                                             
              <p align="center"><font color="#DA004E" face="Arial, Helvetica, sans-serif">                                                                      
              <b>'||p_mensaje||'</b></font></p>                                 
        </td>                                                                   
       </tr>                                                                    
       <tr>                                                                     
       </tr>                                                                    
      </table>                                                                  
      </td>                                                                     
      <td width="5" background="/siicchtm/images/linea_v.gif">'||'&'||'nbsp;</td>                                                                              
      <td width="190" valign="top">                                             
        <span class="secciones">Chavos</span>                                   
        <p><img src="/siicchtm/03Chavos/images/chavos.gif" width="190" height="250" border="0"></p>                                                                      
        </td>                                                                     
    </tr>                                                                       
  </table>                                                                      
</BODY>                                                                         
</HTML>                                                                         
');                                                                             
                                                                                
END;
/
