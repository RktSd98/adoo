import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;

public class uploadImagen extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse res)
                                throws ServletException, IOException {
    res.setContentType("text/html");
    PrintWriter out = res.getWriter();

    String paramID = null;
    String paramTipo = null;
    String paramArchivo = null;
    String paramArchivoAux = null;
    String paramDescripcion = null;
    String paramUrl = null;

    try {
      // Blindly take it on faith this is a multipart/form-data request
      // Construct a MultipartRequest to help read the information.
      // Pass in the request, a directory to saves files to, and the
      // maximum POST size (1 Mb) we should attempt to handle.
        MultipartRequest multi = new MultipartRequest(req, ".",70 * 1024);

        // Recuperamos el nombre de los archivos recividos
        Enumeration files = multi.getFileNames();
        while (files.hasMoreElements()) {
            String name = (String)files.nextElement();
            paramArchivo = multi.getFilesystemName(name);
            String type = multi.getContentType(name);
            File f = multi.getFile(name);
            if ((f == null) || (f.length() <= 0)) {
                out.println("La longitud del archivo debe ser mayor a cero");
            }
        }

        // Recupera los parametros que se recibieron
        Enumeration params = multi.getParameterNames();
        while (params.hasMoreElements()) {
            String name = (String)params.nextElement();

            //Identificamos el nombre del parametro
            if (name.equals("p_id")) {
                paramID = multi.getParameter(name);
            }
            if (name.equals("p_tipo")) {
                paramTipo = multi.getParameter(name);
            }
            if (name.equals("p_archivo_aux")) {
                paramArchivoAux = multi.getParameter(name);
            }
            if (name.equals("p_descripcion")) {
                paramDescripcion = multi.getParameter(name);
            }
            if (name.equals("p_url")) {
                paramUrl = multi.getParameter(name);
            }
        }

        if(paramArchivo == null){
            paramArchivo = paramArchivoAux;
        }

        //Llamamos procedimiento para registrar imagen en la base de datos
res.sendRedirect("/dad/adm_registrar_imagen?p_id=" + paramID + "&p_tipo=" + paramTipo + "&p_archivo=" + paramArchivo + "&p_descripcion="+ paramDescripcion + "&p_url=" + paramUrl);
            }

    catch (Exception e) {
      out.println("<PRE>");
      e.printStackTrace(out);
      out.println("</PRE>");
    }
  }
}
