import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;

public class uploadLocal extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse res)
                                throws ServletException, IOException {
    res.setContentType("text/html");
    PrintWriter out = res.getWriter();

    //Declaracion de variables para recuperar los parametros de entrada
    String paramID = null;
    String paramNumLocal = null;
    String paramTienda = null;
    String paramGerente = null;
    String paramPiso = null;
    String paramTelefono1 = null;
    String paramTelefono2 = null;
    String paramFax = null;
    String paramCoordenadas = null;
    String paramUrl = null;
    String paramEmail = null;
    String paramPalabras = null;
    String paramDescripcion = null;
    String paramArchivo = null;
    String paramArchivoAux = null;

    try {
      // Blindly take it on faith this is a multipart/form-data request
      // Construct a MultipartRequest to help read the information.
      // Pass in the request, a directory to saves files to, and the
      // maximum POST size (1 Mb) we should attempt to handle.
        MultipartRequest multi = new MultipartRequest(req, ".", 40 * 1024);
        
        // Recuperamos el nombre de los archivos recividos
        Enumeration files = multi.getFileNames();
        while (files.hasMoreElements()) {
            String name = (String)files.nextElement();
            paramArchivo = multi.getFilesystemName(name);
            String type = multi.getContentType(name);
            File f = multi.getFile(name);
            if ((f == null) || (f.length() <= 0)) {
                out.println("La longitud del archivo debe ser mayor a cero");
            }
        }

        // Recupera los parametros que se recibieron
        Enumeration params = multi.getParameterNames();
        while (params.hasMoreElements()) {
            String name = (String)params.nextElement();

            //Identificamos el nombre del parametro
            if (name.equals("p_archivo_aux")) {
                paramArchivoAux = multi.getParameter(name);
            }
            if (name.equals("p_id")) {
                paramID = multi.getParameter(name);
            }
            if (name.equals("p_num_local")) {
                paramNumLocal = multi.getParameter(name);
            }
            if (name.equals("p_tienda")) {
                paramTienda = multi.getParameter(name);
            }
            if (name.equals("p_gerente")) {
                paramGerente = multi.getParameter(name);
            }
            if (name.equals("p_piso")) {
                paramPiso = multi.getParameter(name);
            }
            if (name.equals("p_telefono_1")) {
                paramTelefono1 = multi.getParameter(name);
            }
            if (name.equals("p_telefono_2")) {
                paramTelefono2 = multi.getParameter(name);
            }
            if (name.equals("p_fax")) {
                paramFax = multi.getParameter(name);
            }
            if (name.equals("p_coordenadas")) {
                paramCoordenadas = multi.getParameter(name);
            }
            if (name.equals("p_url")) {
                paramUrl = multi.getParameter(name);
            }
            if (name.equals("p_email")) {
                paramEmail = multi.getParameter(name);
            }
            if (name.equals("p_palabras")) {
                paramPalabras = multi.getParameter(name);
            }
            if (name.equals("p_descripcion")) {
                paramDescripcion = multi.getParameter(name);
            }
        }

        if(paramArchivo == null){
            paramArchivo = paramArchivoAux;
        }

        //Llamamos procedimiento para registrar libros en la base de datos
        res.sendRedirect("/dad/adm_registrar_local?p_id=" + paramID + "&p_num_local=" + paramNumLocal + "&p_tienda=" + paramTienda  + "&p_gerente=" + paramGerente + "&p_telefono_1=" + paramTelefono1 + "&p_telefono_2=" + paramTelefono2 + "&p_fax=" + paramFax + "&p_url=" + paramUrl + "&p_email=" + paramEmail + "&p_piso=" + paramPiso + "&p_coordenadas=" + paramCoordenadas + "&p_foto_fachada=" + paramArchivo + "&p_foto_promocion=&p_desc_prom_1=&p_desc_prom_2=" + "&p_descripcion=" + paramDescripcion + "&p_palabras=" + paramPalabras);

    }

    catch (Exception e) {
      out.println("<PRE>");
      e.printStackTrace(out);
      out.println("</PRE>");
    }
  }
}
