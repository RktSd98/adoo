import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;

public class uploadSociales extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse res)
                                throws ServletException, IOException {
    res.setContentType("text/html");
    PrintWriter out = res.getWriter();

    //Declaracion de variables para recuperar los parametros de entrada
    String paramID = null;
    String paramPieFoto = null;
    String paramFechaIni = null;
    String paramFechaFin = null;
    String paramArchivo1 = null;
    String paramArchivo2 = null;
    String paramArchivoAux1 = null;
    String paramArchivoAux2 = null;

    try {
      // Blindly take it on faith this is a multipart/form-data request
      // Construct a MultipartRequest to help read the information.
      // Pass in the request, a directory to saves files to, and the
      // maximum POST size (1 Mb) we should attempt to handle.
        MultipartRequest multi = new MultipartRequest(req, ".", 40 * 1024);
        
        // Recuperamos el nombre de los archivos recividos
        Enumeration files = multi.getFileNames();
        while (files.hasMoreElements()) {
            String name = (String)files.nextElement();

            //Identificamos el nombre del parametro
            if (name.equals("p_archivo_1")) {
                paramArchivo1 = multi.getFilesystemName(name);
            }
            if (name.equals("p_archivo_2")) {
                paramArchivo2 = multi.getFilesystemName(name);
            }

            String type = multi.getContentType(name);
            File f = multi.getFile(name);
            if ((f == null) || (f.length() <= 0)) {
                out.println("La longitud del archivo debe ser mayor a cero");
            }
        }

        // Recupera los parametros que se recibieron
        Enumeration params = multi.getParameterNames();
        while (params.hasMoreElements()) {
            String name = (String)params.nextElement();

            //Identificamos el nombre del parametro
            if (name.equals("p_archivo_aux_1")) {
                paramArchivoAux1 = multi.getParameter(name);
            }
            if (name.equals("p_archivo_aux_2")) {
                paramArchivoAux2 = multi.getParameter(name);
            }
            if (name.equals("p_id")) {
                paramID = multi.getParameter(name);
            }
            if (name.equals("p_pie_foto")) {
                paramPieFoto = multi.getParameter(name);
            }
            if (name.equals("p_fecha_ini")) {
                paramFechaIni = multi.getParameter(name);
            }
            if (name.equals("p_fecha_fin")) {
                paramFechaFin = multi.getParameter(name);
            }
        }

        if(paramArchivo1 == null){
            paramArchivo1 = paramArchivoAux1;
        }
        if(paramArchivo2 == null){
            paramArchivo2 = paramArchivoAux2;
        }

        //Llamamos procedimiento para registrar en la base de datos
res.sendRedirect("/dad/adm_registrar_sociales?p_id=" + paramID + "&p_archivo_1=" + paramArchivo1 + "&p_archivo_2=" + paramArchivo2 + "&p_pie_foto=" + paramPieFoto + "&p_fecha_ini="+ paramFechaIni + "&p_fecha_fin=" + paramFechaFin);

    }

    catch (Exception e) {
      out.println("<PRE>");
      e.printStackTrace(out);
      out.println("</PRE>");
    }
  }
}
