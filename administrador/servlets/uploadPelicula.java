import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;

public class uploadPelicula extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse res)
                                throws ServletException, IOException {
    res.setContentType("text/html");
    PrintWriter out = res.getWriter();

    //Declaracion de variables para recuperar los parametros de entrada
    String paramID = null;
    String paramTitulo = null;
    String paramClasificacion = null;
    String paramDirector = null;
    String paramGuion = null;
    String paramProductor = null;
    String paramEfectos = null;
    String paramMusica = null;
    String paramActores = null;
    String paramDuracion = null;
    String paramGenero = null;
    String paramResenia = null;
    String paramUrl = null;
    String paramFechaIni = null;
    String paramFechaFin = null;
    String paramArchivo = null;
    String paramArchivoAux = null;

    try {
      // Blindly take it on faith this is a multipart/form-data request
      // Construct a MultipartRequest to help read the information.
      // Pass in the request, a directory to saves files to, and the
      // maximum POST size (1 Mb) we should attempt to handle.
        MultipartRequest multi = new MultipartRequest(req, ".", 20 * 1024);
        
        // Recuperamos el nombre de los archivos recividos
        Enumeration files = multi.getFileNames();
        while (files.hasMoreElements()) {
            String name = (String)files.nextElement();
            paramArchivo = multi.getFilesystemName(name);
            String type = multi.getContentType(name);
            File f = multi.getFile(name);
            if ((f == null) || (f.length() <= 0)) {
                out.println("La longitud del archivo debe ser mayor a cero");
            }
        }

        // Recupera los parametros que se recibieron
        Enumeration params = multi.getParameterNames();
        while (params.hasMoreElements()) {
            String name = (String)params.nextElement();

            //Identificamos el nombre del parametro
            if (name.equals("p_archivo_aux")) {
                paramArchivoAux = multi.getParameter(name);
            }
            if (name.equals("p_id")) {
                paramID = multi.getParameter(name);
            }
            if (name.equals("p_titulo")) {
                paramTitulo = multi.getParameter(name);
            }
            if (name.equals("p_clasificacion")) {
                paramClasificacion = multi.getParameter(name);
            }
            if (name.equals("p_director")) {
                paramDirector = multi.getParameter(name);
            }
            if (name.equals("p_guion")) {
                paramGuion = multi.getParameter(name);
            }
            if (name.equals("p_productor")) {
                paramProductor = multi.getParameter(name);
            }
            if (name.equals("p_efectos")) {
                paramEfectos = multi.getParameter(name);
            }
            if (name.equals("p_musica")) {
                paramMusica = multi.getParameter(name);
            }
            if (name.equals("p_actores")) {
                paramActores = multi.getParameter(name);
            }
            if (name.equals("p_duracion")) {
                paramDuracion = multi.getParameter(name);
            }
            if (name.equals("p_genero")) {
                paramGenero = multi.getParameter(name);
            }
            if (name.equals("p_resenia")) {
                paramResenia = multi.getParameter(name);
            }
            if (name.equals("p_url_destino")) {
                paramUrl = multi.getParameter(name);
            }
            if (name.equals("p_fecha_ini")) {
                paramFechaIni = multi.getParameter(name);
            }
            if (name.equals("p_fecha_fin")) {
                paramFechaFin = multi.getParameter(name);
            }
        }

        if(paramArchivo == null){
            paramArchivo = paramArchivoAux;
        }

        //Llamamos procedimiento para registrar peliculas en la base de datos
        res.sendRedirect("/dad/adm_registrar_pelicula?p_id=" + paramID + "&p_archivo=" + paramArchivo + "&p_titulo=" + paramTitulo + "&p_clasificacion=" + paramClasificacion + "&p_director=" + paramDirector + "&p_guion=" + paramGuion + "&p_productor=" + paramProductor + "&p_efectos=" + paramEfectos + "&p_musica=" + paramMusica + "&p_actores=" + paramActores + "&p_duracion=" + paramDuracion + "&p_genero=" + paramGenero + "&p_resenia=" + paramResenia + "&p_url_destino=" + paramUrl + "&p_fecha_ini="+ paramFechaIni + "&p_fecha_fin=" + paramFechaFin);

    }

    catch (Exception e) {
      out.println("<PRE>");
      e.printStackTrace(out);
      out.println("</PRE>");
    }
  }
}
